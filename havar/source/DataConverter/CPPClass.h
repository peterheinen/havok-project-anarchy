#include <stdio.h>

class CPPClass
{
public:
  CPPClass(void) {};
public:
  ~CPPClass(void) {};

  void produceByteArray(unsigned char* array, int length)
  {
    printf("CPPClass: Producing %i elements\n", length);
    for (int i=0; i<length; i++)
    {
      array[i] = i*2;
    }
  }
};
