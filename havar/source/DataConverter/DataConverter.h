#pragma once

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <list>
#include "Entity.h"

#import <mscorlib.tlb>
#import "..\Havar\bin\Release\Havar.tlb" no_namespace named_guids

class DataConverter {
private:

public:
	DataConverter();
	~DataConverter();
	
	bool isInitializedCorrectly;
	bool failed;

	std::vector<double>				getTableClipBox();
	std::vector<double>				getCameraData();
	std::map<std::string, Entity*>	getPoIs();
	
	std::map<std::string, Entity*>	entities;
	std::list<Entity*>				updateObjectList;

	std::list<Entity*> getUpdatedEntities();
	void addEntity(std::string);
	void createUpdateEntityObject(std::string);
	void Init(std::string ipAddres, int port);

	IHavarInterface *cpi;
};