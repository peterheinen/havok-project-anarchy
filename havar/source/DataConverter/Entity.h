#pragma once

#include <string>

// Data container for entities
class Entity
{
public:
	Entity(std::string guid, std::string name, std::string type, double latitude, double longitude, double altitude, double orientation);
	Entity(std::string guid, std::string name, std::string type, double latitude, double longitude, double altitude, double orientation, std::string animation);
	~Entity();

	std::string name, type, entityString, guid, animation, extras;
	double latitude, longitude, altitude, orientation;
	bool isActive;

	std::string getString();
};

