#pragma once

struct Double3D {
	float x;
	float y;
	float z;
};

class ExtraMath
{
public:
	ExtraMath(void);
	~ExtraMath(void);

	Double3D currentRefPos;

	Double3D *positionToLocalENU(Double3D pPos, Double3D refPos);
	double groundRangeTo(Double3D pPos);
	double headingTo(Double3D pPos);
	double aCos(double a);
	double angleZero2PI(double x);
	double angleMinPIPlusPI(double px);
};

