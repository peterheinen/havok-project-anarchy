#include "Entity.h"
#include <sstream>

Entity::Entity(std::string guid, std::string name, std::string type, double latitude, double longitude, double altitude, double orientation, std::string extras)
{
	animation = "";
	isActive = true; 

	this->guid		= guid;
	this->name		= name;
	this->type		= type;
	this->latitude	= latitude;
	this->longitude	= longitude;
	this->altitude	= altitude;
	this->orientation = orientation;
	this->extras = extras;	
}


Entity::~Entity()
{
}
	
std::string Entity::getString() {
	return entityString;
}