﻿using System;
using System.Runtime.InteropServices;

using Havar;

namespace Havar
{
    // Since the .NET Framework interface and coclass have to behave as 
    // COM objects, we have to give them guids.
    [Guid("DBE0E8C4-1C61-41f3-B6A4-4E2F353D3D05")]
    public interface IHavarInterface
    {
        int PrintHi(string name);
        double[] GetCameraPosition();
        bool InitHavar();
        void ShutDownHavar();
        String GetFullPoIString();
        String GetUpdateList();
        bool IsCameraChanged();
        bool IsPoIChanged();
        bool ShouldUpdate();
    }

    [Guid("C6659361-1625-4746-931C-36014B146679")]
    public class InterfaceImplementation : IHavarInterface
    {
        private Havar hav;

        public bool InitHavar()
        {
            hav = new Havar();
            return hav.Connect();
        }

        public void ShutDownHavar()
        {
            hav.CloseWindow();
            hav = null;
        }

        public String GetFullPoIString()
        {
            return hav.getFullPoIString();
        }

        public String GetUpdateList()
        {
            return hav.GetUpdateList();
        }

        public double[] GetCameraPosition()
        {
            return hav.getCameraPosition();
        }

        public bool IsCameraChanged()
        {
            return hav.isCameraChanged();
        }

        public bool IsPoIChanged()
        {
            return hav.isPoIChanged();
        }

        public bool ShouldUpdate()
        {
            return hav.shouldUpdate();
        }

        public int PrintHi(string name)
        {
            Console.WriteLine("Hello, {0}!", name);
            return 33;
        }
    }
}