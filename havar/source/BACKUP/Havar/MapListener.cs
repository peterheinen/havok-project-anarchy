﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havar
{
    class MapListener
    {
        private double tableWidth   = 0.89;     // 89 cm
        private double tableHeight  = 0.50;     // 50 cm
        private string eventName    = "fastmapextent";

        private double[]                    mapExtendCoords;
        private csImb.csImb                 mImbClient;
        private List<csImb.ImbClientStatus> mClients = new List<csImb.ImbClientStatus>();           // Mirrored list
        private List<IMB3.TEventEntry>      mMapExtentEventsCollection = new List<IMB3.TEventEntry>();   // Mirrored list

        private bool cameraChanged = false;

        /// Constructor
        public MapListener(csImb.csImb imbClient)
        {
            mImbClient = imbClient;

            mapExtendCoords = new double[4];

            for(int i = 0; i < 4; i++)
                mapExtendCoords[i] = i;
        }

        /// <summary>
        /// Start listening to the map of this client. Subscribe for 'fastmapextent' events.
        /// </summary>
        /// <param name="client">The client to start listening to</param>
        public void Start(csImb.ImbClientStatus client)
        {
            Console.Out.WriteLine("MapListener: Subscribing to {0}.{1}", client.Id, eventName);

            // Subscribe for Map Extent events
            IMB3.TEventEntry mapExtentEvents = mImbClient.Imb.Subscribe(client.Id + "." + eventName);
            mapExtentEvents.OnNormalEvent += ImbMapExtentEvent;

            // Add client and map extent event entry to list
            mClients.Add(client);
            mMapExtentEventsCollection.Add(mapExtentEvents);
        }

        /// <summary>
        /// Stop listening to the map of this client. Unsubscribe for 'fastmapextent' events.
        /// </summary>
        /// <param name="client">The client to stop listening to</param>
        public void Stop(csImb.ImbClientStatus client)
        {
            Console.Out.WriteLine("MapListener: Unsubscribing from {0}.{1}", client.Id, eventName);

            int index = UnsubscribeClient(client);
            if (index >= 0)
            {
                // Remove client from list
                mClients.RemoveAt(index);
                mMapExtentEventsCollection.RemoveAt(index);
            }
        }

        /// <summary>
        /// Stop listening to the maps of all clients. Unsubscribe for 'fastmapextent' events.
        /// </summary>
        public void StopAll()
        {
            foreach (csImb.ImbClientStatus client in mClients)
            {
                Console.Out.WriteLine("MapListener: Unsubscribing from {0}.{1}", client.Id, eventName);
                UnsubscribeClient(client);
            }

            mClients.Clear();
            mMapExtentEventsCollection.Clear();
        }

        /// Util: Unsubscribe  
        private int UnsubscribeClient(csImb.ImbClientStatus client)
        {
            int index = mClients.IndexOf(client);
            if (index >= 0)
            {
                IMB3.TEventEntry mapExtentEvents = mMapExtentEventsCollection.ElementAt<IMB3.TEventEntry>(index);
                mapExtentEvents.OnNormalEvent -= ImbMapExtentEvent;
            }

            // Unsubscribe from Map Extent events
            mImbClient.Imb.UnSubscribe(client.Id + "." + eventName);

            return index;
        }

        /// Map extent event occured
        private void ImbMapExtentEvent(IMB3.TEventEntry aEvent, IMB3.ByteBuffers.TByteBuffer aPayload)
        {
            string payloadStr = aPayload.ReadString();

            // Split payload (get coordinates)
            char[] payloadDelimiters = { '|' };
            string[] coordinateStrs = payloadStr.Split(payloadDelimiters);

            mapExtendCoords = new double[4];
            
            double[] coordinates;

            if (ParseStringsToDoubles(coordinateStrs, out coordinates))
            {
                mapExtendCoords = coordinates;
                cameraChanged = true;
            }
        }

        public double[] getMapExtendCoords()
        {
            cameraChanged = false;
            return mapExtendCoords;
        }

        public bool isCameraChanged()
        {
            return cameraChanged;
        }

        public static bool ParseStringsToDoubles(string[] strs, out double[] doubles)
        {
            doubles = new double[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                if (!Double.TryParse(strs[i], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out doubles[i]))
                    return false;
            }
            return true;
        }
    }
}
