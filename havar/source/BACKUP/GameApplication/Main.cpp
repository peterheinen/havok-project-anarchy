/*
 *
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Product and Trade Secret source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2013 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 *
 */


//  Basic template to base a project EXE on.
//	This template version contain a simple demo Action in the TemplateAction.cpp file
//
//	TODO: Needs config file for properties
//

#include "GameApplicationPCH.h"
#include <string>
#include <iostream>
#include <hkvQuat.h> 
#include <stdlib.h>
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/Scene/VPrefab.hpp>
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/GUI/vGUI.hpp>
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/GUI/VWindowBase.hpp>

//ALVAR includes
#include "CaptureFactory.h" // Video capturing
#include "MarkerDetector.h" // Marker detector
#include "MultiMarker.h"
#include "DataConverter.h"

#include "Shared.h"

//============================================================================================================
//	Properties for start up. Some of the settings are not relevant for mobile devices
//============================================================================================================
int windowSizeX		= 1024;									// Set the Window size X if not in fullscreen.
int windowSizeY		= 768;									// Set the Window size Y if not in fullscreen.
int windowPosX		= 500;									// Set the Window position X if not in fullscreen.
int windowPosy		= 50;									// Set the Window position Y if not in fullscreen.

char name[]			= "TNO Havok";					// Name to be displayed in the windows title bar.
char StartUpScene[]	= "EpicTest.vscene";		// Set the location of your start up scene.

int videoXRes = 0; // Video capture width (x)
int videoYRes = 0; // video capture height (y)

const double TABLE_WIDTH	= 0.25;    // 89 cm
const double TABLE_HEIGHT	= 0.15;   // 50 cm

std::vector<double> clipBox(4);
std::map<std::string, Entity*> entities;
std::map<std::string, VisBaseEntity_cl*> gameObjects;
std::list<VisBaseEntity_cl*> rangeObjects;
std::list<std::string> types;
std::string myTypes;

bool boom = true, initAmsterdam = true;
int yawoffset, pitchoffset, rolloffset;
float yOffset, xOffset, scaling = 0.01f;
double xOffsetModels = 690342, yOffsetModels = 7042842;
double xref, yref;

alvar::Capture*	capture = NULL; // ALVAR Capture

alvar::Camera								camera;
alvar::MarkerDetector<alvar::MarkerData>	markerDetector;
alvar::Pose pose;
alvar::MultiMarker *multi_marker;		
DataConverter *dc;
bool detect_additional = true, showRanges = true, showAirCorridors = true;

VisBaseEntity_cl* amsterdam = NULL;
VisBaseEntity_cl* cameraCorrector = NULL;
VPrefab *pPrefab;
VPrefab *amsterdamPrefab;
VisBaseEntity_cl *pCamera; 

VSmartPtr<VGUIMainContext> spGUIContext;
VDialogPtr spMainDlg;

// Size of the marker
// The size is an "abstract value" for library, but using normal, logical values (mm, cm, m, inch) will help 
// understanding the model scaling and positioning in human point of view.
#define MARKER_SIZE	4 //as in centimeters
const int nof_markers = 5;

//use the following line if you link statically. e.g. for mobile. 
//You can remove this line when developing for windows only
VIMPORT IVisPlugin_cl* GetEnginePlugin_GamePlugin();

//============================================================================================================
//	Action  Declaration  
//============================================================================================================
//	Declare a module to put all our actions into
	DECLARE_THIS_MODULE(g_sampleModule, MAKE_VERSION(1,0),
										"Project Template Module",
										"Trinigy",
										"This module contains this projects actions", NULL );

//	If you need to access the module somewhere else, just include this line but never declare multiple times: 
//	only one declare in one .cpp file! 
//	extern VModule g_sampleModule; 

                    
VisSampleAppPtr spApp;

class BackgroundImageHelper : public IVisCallbackHandler_cl
{
public:
  BackgroundImageHelper( VTextureObject* pTexture )
    : IVisCallbackHandler_cl()
    , m_spTexture( pTexture )
  {
    v2TopLeft = hkvVec2::ZeroVector();

    // Determine the bottom right corner for our fullscreen quad.
    VVideoConfig *pVideoConfig = Vision::Video.GetCurrentConfig();
    v2BottomRight = hkvVec2( (float)pVideoConfig->m_iXRes, (float)pVideoConfig->m_iYRes );

    // Listen to render hook callbacks.
    Vision::Callbacks.OnRenderHook += this;
  }

  virtual ~BackgroundImageHelper()
  {
    // Unregister from render hook callbacks.
    Vision::Callbacks.OnRenderHook -= this;
  }

  VTextureObject* GetTexture() const
  {
    return m_spTexture;
  }

  void OnHandleCallback( IVisCallbackDataObject_cl* pData ) HKV_OVERRIDE
  {
    if ( pData->m_pSender == &Vision::Callbacks.OnRenderHook )
    {
      VisRenderHookDataObject_cl* pRenderHookData = ( VisRenderHookDataObject_cl* ) pData;

      // Only perform rendering in the pre-occlusion test pass
      if ( pRenderHookData->m_iEntryConst != VRH_PRE_OCCLUSION_TESTS )
      {
        return;
      }

      // Draw our texture as fullscreen quad.
      IVRender2DInterface *pRenderer = Vision::RenderLoopHelper.BeginOverlayRendering();
      pRenderer->DrawTexturedQuad( v2TopLeft, v2BottomRight, m_spTexture, hkvVec2( 0.0f, 0.0f ), hkvVec2( 1.0f, 1.0f ), V_RGBA_WHITE, RENDERSTATEFLAG_NOWIREFRAME );
      Vision::RenderLoopHelper.EndOverlayRendering();
    }
  }

private:
  VTextureObjectPtr m_spTexture;
  hkvVec2 v2TopLeft;
  hkvVec2 v2BottomRight;
};

void InitGUI()
{
	spGUIContext = new VGUIMainContext(NULL);
	spGUIContext->SetActivate(true);

	// Load some default resources (like fonts or the image for the cursor)
	VGUIManager::GlobalManager().LoadResourceFile("Dialogs\\MenuSystem.xml");

	// Load and show the Main Menu layout
	spMainDlg = spGUIContext->ShowDialog("Dialogs\\MainMenu.xml");
	spMainDlg->SetVisible(false);

	VASSERT(spMainDlg);
}

void DeInitGUI()
{
  spMainDlg = NULL;                  // destroy the Main Dialog object
  spGUIContext->SetActivate(false);  // deactivate the GUI context ...
  spGUIContext = NULL;               // ... and destroy it
}

//---------------------------------------------------------------------------------------------------------
//	Init function. Here we trigger loading our scene
//---------------------------------------------------------------------------------------------------------
VISION_INIT
{
  	//	Create our new application.
	spApp = new VisSampleApp();
	
	//	set the initial starting position of our game window
	//	and other properties if not in fullscreen. This is only relevant on windows
#if defined(WIN32)
  spApp->m_appConfig.m_videoConfig.m_iXPos = windowPosX;
	spApp->m_appConfig.m_videoConfig.m_iYPos = windowPosy;
	spApp->m_appConfig.m_videoConfig.m_szWindowTitle = name;
#endif

	//	Set the exe directory the current directory
	VisionAppHelpers::MakeEXEDirCurrent();

	// Set the paths to our stand alone version to over ride the VisSAampleApp paths.
  // The paths are platform dependent
#if defined(WIN32)
  const VString szRoot = "..\\..\\..\\..";
  //Vision::File.AddDataDirectory( szRoot );
  Vision::File.AddDataDirectory( szRoot + "\\Assets" );
  Vision::File.AddDataDirectory( szRoot + "\\Data\\Vision\\Base" );
  Vision::File.AddDataDirectory( szRoot + "\\Assets\\Models\\Misc\\Textures" );
#elif defined(_VISION_ANDROID)
  VString szRoot = VisSampleApp::GetApkDirectory();
  szRoot += "?assets";
  Vision::File.AddDataDirectory( szRoot + "\\Assets" );
  //Vision::File.AddDataDirectory( szRoot + "/Data/Vision/Base" ); //we don't have to call this line, since the base data 
                                                                   //folder is added automatically in the VisSampleApp init function
#endif


  //load plugins dynamically (windows only)
	#if defined(_DLL) && !defined(VBASE_LIB)
		//Load the plugins that the scene referenced.
		VSceneLoader ldr;
		ldr.LoadEnginePlugins( StartUpScene, Vision::File.GetManager() );
	#endif

	spApp->LoadVisionEnginePlugin();

  //use the following line if you link statically. e.g. for mobile. 
  //You can remove this line when developing for windows only
  VISION_PLUGIN_ENSURE_LOADED(GamePlugin);

	//	Init the application and point it to the start up scene.
	if (!spApp->InitSample( "", StartUpScene, VSAMPLE_INIT_DEFAULTS|VSAMPLE_CUSTOMDATADIRECTORIES,windowSizeX,windowSizeY))
    return false;

  return true;
}

//---------------------------------------------------------------------------------------------------------
//	Gets called after the scene has been loaded
//---------------------------------------------------------------------------------------------------------
BackgroundImageHelper *pBackgroundImageHelper = NULL;

VISION_SAMPLEAPP_AFTER_LOADING
{
    //	define some help text
    spApp->AddHelpText( "" );
    spApp->AddHelpText( "How to use this demo :" );
    spApp->AddHelpText( "" );

    //	Create a mouse controlled camera (optionally with gravity)
    pCamera = spApp->EnableMouseCamera();
    pCamera->SetPosition( hkvVec3(6010, 4030, 70));
	pCamera->SetOrientation( hkvVec3( 60, 20, 0));

	IVRendererNode* pRenderer = Vision::Renderer.GetRendererNode(0); 
	pRenderer->GetViewProperties()->setFov(45, 0);
	pRenderer->OnViewPropertiesChanged();

	alvar::CaptureFactory *factory = alvar::CaptureFactory::instance();
    alvar::CaptureFactory::CaptureDeviceVector devices = factory->enumerateDevices();

    // Check to ensure that a device was found
    if (devices.size() > 0) {
        capture = factory->createCapture(devices.front());
    }

	cameraCorrector		= Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3 (0,0,0), "");
	cameraCorrector->SetScaling(0.0f);
	cameraCorrector->SetOrientation(0, 0, 0);

	//pCamera->SetOrientation(90, -90, 0);
	//pCamera->AttachToParent(cameraCorrector);

	dc = new DataConverter();
	dc->init();
	InitGUI();

	xref = 53.35f;
	yref = 6.17f;
	
	/*amsterdamPrefab = VPrefabManager::GlobalManager().LoadPrefab("Prefabs\\Amsterdam.vprefab");
	
	pPrefab			= VPrefabManager::GlobalManager().LoadPrefab("Prefabs\\ExplosionImpact.vprefab");
	

	// Capture is central feature, so if we fail, we get out of here.
    if (capture && capture->start()) {
		// Let's capture one frame to get video resolution
		IplImage *tempImg = capture->captureImage();
		
		//Create space for outputs rgb and its separate channels, r, g and b
		IplImage* goodImg = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 4);    //rgb
		IplImage* r = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //r
		IplImage* g = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //g
		IplImage* b = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //b
		IplImage* a = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //b

		cvSplit(tempImg, b, g, r, NULL);
		cvMerge(r,g,b,a,goodImg);

		videoXRes = goodImg->width;
		videoYRes = goodImg->height;

		// Create an empty texture and fill it with 32bit image data from memory.
		VTextureObject *pTexture = Vision::TextureManager.CreateEmpty2DTextureObject( "<background>", videoXRes, videoYRes );
		pTexture->EnsureLoaded();
		pTexture->UpdateRect( 0, 0, 0, videoXRes, videoYRes, -1, goodImg->imageData, V_TEXTURE_LOCKFLAG_DISCARDABLE );

		// Create a background image helper.
		pBackgroundImageHelper = new BackgroundImageHelper( pTexture );

		cvReleaseImage(&goodImg);
		cvReleaseImage(&r);
		cvReleaseImage(&g);
		cvReleaseImage(&b);
		cvReleaseImage(&a);

		// Calibration. See manual and ALVAR internal samples how to calibrate your camera
		// Calibration will make the marker detecting and marker pose calculation more accurate.
		if (! camera.SetCalib("calib.xml", videoXRes, videoYRes)) {
			camera.SetRes(videoXRes, videoYRes);
		}
	}*/

    //---------------------------------------------------------------------------------------------------------
    //	register the action module with the vision engine action manager
    //	only after that will the action becomes available in the console.
    //---------------------------------------------------------------------------------------------------------
    VActionManager * pManager = Vision::GetActionManager ();
    pManager->RegisterModule ( &g_sampleModule );

    //	Set to true to open the console at startup and print some data to the display
    Vision::GetConsoleManager()->Show( false );
    pManager->Print( "Welcome to the console!" );
    pManager->Print( "This module is called '%s'", g_sampleModule.GetName() );
    pManager->Print( "Type in 'help' for a list of all actions" );
    pManager->Print( "Type in 'myAction' to test this projects demo action" );
}

//---------------------------------------------------------------------------------------------------------
//	main loop of the application until we quit
//---------------------------------------------------------------------------------------------------------
VISION_SAMPLEAPP_RUN
{
	/*double realWidthDistance =  clipBox[2] - clipBox[0];
    double realHeightDistance = clipBox[3] - clipBox[1];
    double scaleFactor = ((realHeightDistance / TABLE_HEIGHT) + (realWidthDistance / TABLE_WIDTH)) / 2;

	std::vector<int> id_vector;
    for(int i = 0; i < nof_markers; ++i)
        id_vector.push_back(i);

	// Tell the ALVAR the markers' size (same to all)
	// You can also specify different size for each individual markers
	markerDetector.SetMarkerSize((MARKER_SIZE * scaleFactor) / 100);

    markerDetector.SetMarkerSizeForId(0, (MARKER_SIZE * scaleFactor) / 100 * 2);
    multi_marker = new MultiMarker(id_vector);
    pose.Reset();
    multi_marker->PointCloudAdd(0, (MARKER_SIZE * scaleFactor) / 100 * 2, pose);
    pose.SetTranslation(-(MARKER_SIZE * scaleFactor) / 100*2.5, +(MARKER_SIZE * scaleFactor) / 100*1.5, 0);
    multi_marker->PointCloudAdd(1, (MARKER_SIZE * scaleFactor) / 100, pose);
    pose.SetTranslation(+(MARKER_SIZE * scaleFactor) / 100*2.5, +(MARKER_SIZE * scaleFactor) / 100*1.5, 0);
    multi_marker->PointCloudAdd(2, (MARKER_SIZE * scaleFactor) / 100, pose);
    pose.SetTranslation(-(MARKER_SIZE * scaleFactor) / 100*2.5, -(MARKER_SIZE * scaleFactor) / 100*1.5, 0);
    multi_marker->PointCloudAdd(3, (MARKER_SIZE * scaleFactor) / 100, pose);
    pose.SetTranslation(+(MARKER_SIZE * scaleFactor) / 100*2.5, -(MARKER_SIZE * scaleFactor) / 100*1.5, 0);
    multi_marker->PointCloudAdd(4, (MARKER_SIZE * scaleFactor) / 100, pose);

	//Call the rendering function over and over again.
	IplImage *image = capture->captureImage(); 
	
	// Check if we need to change image origin and is so, flip the image.
	bool flip_image = (image->origin?true:false);
	if (flip_image) {
		cvFlip(image);
		image->origin = !image->origin;
	}

	int visualize=0;
	double error=-1;
    if (markerDetector.Detect(image, &camera, true, (visualize == 1), 0.0)) {
        if (detect_additional) {
            error = multi_marker->Update(markerDetector.markers, &camera, pose);
            multi_marker->SetTrackMarkers(markerDetector, &camera, pose, visualize ? image : NULL);
            markerDetector.DetectAdditional(image, &camera, (visualize == 1));
        }
        if (visualize == 2) 
            error = multi_marker->Update(markerDetector.markers, &camera, pose, image);
        else 
            error = multi_marker->Update(markerDetector.markers, &camera, pose);
    }

	if ((error >= 0) && (error < 5)) {		 
		CvMat *invertedMatrix = cvCreateMat(4, 4, CV_64FC1); 
		pose.GetMatrix(invertedMatrix);
	 
		hkvMat4* havokMatrix = new hkvMat4(cvmGet(invertedMatrix, 0, 0),cvmGet(invertedMatrix, 0, 1),cvmGet(invertedMatrix, 0, 2),cvmGet(invertedMatrix, 0, 3),
			cvmGet(invertedMatrix, 1, 0),cvmGet(invertedMatrix, 1, 1),cvmGet(invertedMatrix, 1, 2),cvmGet(invertedMatrix, 1, 3),
			cvmGet(invertedMatrix, 2, 0),cvmGet(invertedMatrix, 2, 1),cvmGet(invertedMatrix, 2, 2),cvmGet(invertedMatrix, 2, 3),
			cvmGet(invertedMatrix, 3, 0),cvmGet(invertedMatrix, 3, 1),cvmGet(invertedMatrix, 3, 2),cvmGet(invertedMatrix, 3, 3));

		havokMatrix->invert();

		pCamera->SetLocalOrientation(90, -90, 0);
		pCamera->AttachToParent(cameraCorrector);

		float yaw, pitch, roll;
		havokMatrix->getAsEulerAngles(roll,pitch,yaw);
		hkvVec3 position = havokMatrix->getTranslation();
		
		cameraCorrector->SetOrientation(yaw, pitch, roll); 
		pCamera->SetPosition(position.x + xOffset, position.y + yOffset, position.z);

		for(std::list<VisBaseEntity_cl*>::iterator ii = rangeObjects.begin(); ii != rangeObjects.end(); ++ii ) {
			if(showRanges) {
				(*ii)->SetScaling(1.0f);
			} else {
				(*ii)->SetScaling(0.0f);
			}
		}
		
        for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = gameObjects.begin(); ii != gameObjects.end(); ++ii ) {
            (*ii).second->SetScaling(scaling);
                                
            if((*ii).second->GetPosition().z > 0) {
                    Vision::Game.DrawSingleLine((*ii).second->GetPosition().x,(*ii).second->GetPosition().y, 0,
                            (*ii).second->GetPosition().x,(*ii).second->GetPosition().y,(*ii).second->GetPosition().z, V_RGBA_RED);
            }
        }
	}		

	// In case we flipped the image, it's time to flip it back 
	if (flip_image) {
		cvFlip(image);
		image->origin = !image->origin;
	}	

	uint64 uiStartTime = VGLGetTimer();
	
	//Create space for outputs rgb and its separate channels, r, g and b
	IplImage* goodImg = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 4);    //rgb
	IplImage* r = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //r
	IplImage* g = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //g
	IplImage* b = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //b
	IplImage* a = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //a

	cvSplit(image, b, g, r, NULL);
	cvMerge(b, g, r, a, goodImg);

	pBackgroundImageHelper->GetTexture()->UpdateRect( 0, 0, 0, goodImg->width, goodImg->height, -1, goodImg->imageData, V_TEXTURE_LOCKFLAG_DISCARDABLE );
	uint64 uiEndTime = VGLGetTimer();

	cvReleaseImage(&goodImg);
	cvReleaseImage(&r);
	cvReleaseImage(&g);
	cvReleaseImage(&b);
	cvReleaseImage(&a);

	float fDuration = ( uiEndTime - uiStartTime ) / float( VGLGetTimerResolution() ) * 1000.0f;
		*/	
	if(dc->cpi->IsCameraChanged()) {
		clipBox = dc->getTableClipBox();

		float xTopLeft = clipBox[0];
		float yTopLeft = clipBox[3];
		float xBottomRight = clipBox[2];
		float yBottomRight = clipBox[1];

		xOffset = (xTopLeft + (xBottomRight - xTopLeft) / 2) - 697000;
		yOffset = (yBottomRight + (yTopLeft - yBottomRight) / 2) - 7050000;
	}										
	
	if(dc->cpi->IsPoIChanged()) {
		entities = dc->getPoIs();
		
		for(std::map<std::string, Entity*>::iterator ii = entities.begin(); ii != entities.end(); ++ii ) {
			if(gameObjects.find((*ii).second->guid) == gameObjects.end()) {
				VisBaseEntity_cl* tmpObject;
				std::string modelName;
			
				if((*ii).second->type == "Man") {
					modelName.append("Models\\Misc\\Eddy.MODEL" );					
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());

					VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
						VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, "Talk");
				} else if((*ii).second->type == "Woman") {
					modelName.append("Models\\Misc\\female_008_high.model" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());

				} else if((*ii).second->type == "Soldier") {
					modelName.append("Models\\Misc\\soldier_high.MODEL" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());

					VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
						VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, "Walk");
				} else if((*ii).second->type == "Helicopter") {
					modelName.append("Models\\Misc\\ah-64-armed.model" );
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
				} else if((*ii).second->type == "Child") {
					modelName.append("Models\\Misc\\female_008_high.model" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
				} else if((*ii).second->type == "Aeroplane") {
					modelName.append("Models\\Misc\\boeing_737.model" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
				} else if((*ii).second->type == "Tank") {
					modelName.append("Models\\Misc\\leopard2a6.model" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
				} else if((*ii).second->type == "StaticWeapon") {
					modelName.append("Models\\Misc\\sentinel.model" );
					VisBaseEntity_cl* rangeObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), "Models\\Misc\\dome.model");		
					rangeObject->SetScaling(1);
					
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
					rangeObject->AttachToParent(tmpObject);
					rangeObjects.push_back(rangeObject);
				} else if((*ii).second->type == "Jeep") {
					modelName.append("Models\\Misc\\fennek_des.model" );
									
					tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3((*ii).second->longitude - xOffsetModels, (*ii).second->latitude - yOffsetModels, (*ii).second->altitude), modelName.c_str());
				}

				tmpObject->SetOrientation((*ii).second->orientation - 90, 0,0);
				tmpObject->SetScaling(scaling);
				gameObjects.insert(std::pair<std::string, VisBaseEntity_cl*>((*ii).second->guid, tmpObject));
			}
		}
	}

	/*if(initAmsterdam) {
		initAmsterdam = false;
		VPrefabInstanceInfo info;
		info.m_vInstancePos.SetXYZ(0,5000,0);	// change position for every instance
		info.m_vInstanceEuler.SetXYZ(0,0,0);	// yaw orientation in steps of 45 degrees
		info.m_bOutputInstances = false;		// we do not need a list of created objects
		amsterdam = Vision::Game.CreateEntity("VisBaseEntity_cl", info.m_vInstancePos);
		info.m_pParentObject = amsterdam;
		amsterdamPrefab->Instantiate(info);

		VisBaseEntity_cl* tmpObject;
		tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3(0, 5000, 3), "Models\\Misc\\landing.model");
		
	}*/
	
	int dialogResult = spMainDlg->GetDialogResult();
	
	if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWRANGES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX1"));
		VASSERT(checkbox);
		showRanges = checkbox->IsChecked();
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWDROPLINES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX2"));
		VASSERT(checkbox);
		showAirCorridors = checkbox->IsChecked();
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SCALE")) {
		VSliderControl *slider = (VSliderControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("WALKSTAND_SLIDER"));
		VASSERT(slider);
		scaling = slider->GetValue() / 100;
	}	
	
	if(Vision::Key.IsPressed(VGLK_P)) {
		spMainDlg->SetVisible(true);
	} else if(Vision::Key.IsPressed(VGLK_O) || (dialogResult == VGUIManager::GlobalManager().GetID("HIDEGUI"))) {
		spMainDlg->SetVisible(false);
		spMainDlg->SetDialogResult(VGUIManager::ID_OK);
	}

	//gameObjects.insert(std::pair<std::string, VisBaseEntity_cl*>("Terrain", amsterdam));
	float changefactor = 0.1f;
	if(Vision::Key.IsPressed(VGLK_M))
		xOffsetModels += changefactor;
	if(Vision::Key.IsPressed(VGLK_N)) {
		xOffsetModels -= changefactor;
	}
	if(Vision::Key.IsPressed(VGLK_U))
		yOffsetModels += changefactor;
	if(Vision::Key.IsPressed(VGLK_I)) {
		yOffsetModels -= changefactor;
	}

	if(Vision::Key.IsPressed(VGLK_K))
		scaling += 0.0001f;
	if(Vision::Key.IsPressed(VGLK_L)) {
		if(scaling > 0.01f)
			scaling -= 0.0001f;
		
		if(scaling < 0.01f)
			scaling = 0.01f;
	}

	Vision::Message.Print(1, 10, 20, "Offset: %f %f \n", xOffsetModels, yOffsetModels);
	Vision::Message.Print(1, 10, 40, "Camera Position: %f %f %f \n", pCamera->GetPosition().x, pCamera->GetPosition().y, pCamera->GetPosition().z);
	if(gameObjects.size() != 0)
	Vision::Message.Print(1, 10, 60, "Stuff position Position: %f %f %f \n", gameObjects.begin()->second->GetPosition().x,  gameObjects.begin()->second->GetPosition().y,  gameObjects.begin()->second->GetPosition().z);
	Vision::Message.Print(1, 10, 80, "Scaling: %f", scaling);
	Vision::Message.Print(1, 10, 100, "Size: %i", gameObjects.size());
	/*dc->referencePoint.x = xref;
	dc->referencePoint.y = yref;
	dc->referencePoint.z = 0.0f;*/
	
	if(dc->cpi->ShouldUpdate()) {
		std::list<Entity*> updateObjectList = dc->getUpdatedEntities();

		for(std::list<Entity*>::iterator poiIterator = updateObjectList.begin(); poiIterator != updateObjectList.end(); ++poiIterator ) {
			for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = gameObjects.begin(); ii != gameObjects.end(); ++ii ) {
				if((*ii).first == ((*poiIterator)->guid)) {
					(*ii).second->SetPosition((*poiIterator)->longitude - xOffsetModels, (*poiIterator)->latitude - yOffsetModels, (*poiIterator)->altitude);
					(*ii).second->SetOrientation(-(*poiIterator)->orientation, 0, 0);
					(*ii).second->SetScaling(scaling);
				}
			}		
		}
	}

    return spApp->Run() && dialogResult != VGUIManager::ID_CANCEL;
}


VISION_DEINIT
{
	// Time to close the system
	/*if(capture){
		capture->stop();
		delete capture;
	}

	delete pBackgroundImageHelper;
	pBackgroundImageHelper = NULL;
	*/
	DeInitGUI();
    
	// Deinit the application
    spApp->DeInitSample();
    spApp = NULL;
    return true;
}

VISION_MAIN_DEFAULT

/*
 * Havok SDK - Base file, BUILD(#20130523)
 * 
 * Confidential Information of Havok.  (C) Copyright 1999-2013
 * Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
 * Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
 * rights, and intellectual property rights in the Havok software remain in
 * Havok and/or its suppliers.
 * 
 * Use of this software for evaluation purposes is subject to and indicates
 * acceptance of the End User licence Agreement for this product. A copy of
 * the license is included with this software and is also available from salesteam@havok.com.
 * 
 */