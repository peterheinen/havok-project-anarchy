#include <windows.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include "DataConverter.h"
#include <string>
#include <sstream>

#define _USE_MATH_DEFINES
#include <math.h>

#pragma warning (disable: 4278)

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

DataConverter::DataConverter() {
	EQUATOR_RADIUS = 6371100.0f;
	extraMath = new ExtraMath();
}

DataConverter::~DataConverter() {
	cpi->ShutDownHavar();
    cpi->Release();
    cpi = NULL;
	CoUninitialize();
}

void DataConverter::init() {
	isInitializedCorrectly = false;
	CoInitialize(NULL);
	
	HRESULT hr = CoCreateInstance(CLSID_InterfaceImplementation,
				NULL, CLSCTX_INPROC_SERVER,
				IID_IHavarInterface, reinterpret_cast<void**>(&cpi));

	if (FAILED(hr))
		printf("Couldn't create the instance!... 0x%x\n", hr);
	else
		isInitializedCorrectly = cpi->InitHavar();
}

double Asinh(double x) {
	return log(x + sqrt(x * x + 1));
}

std::vector<double> DataConverter::getTableClipBox() {	
	std::vector<double> myvector(4);
	SAFEARRAY *someSafeArray = cpi->GetCameraPosition();

	double HUGEP *pData;
	HRESULT hr = SafeArrayAccessData(someSafeArray, (void HUGEP* FAR*)&pData);

	if (SUCCEEDED(hr))
		for (int i = 0; i < 4; i++)
			myvector[i] = pData[i];

	SafeArrayUnaccessData(someSafeArray);
	SafeArrayDestroy(someSafeArray);

	return myvector;
}

std::map<std::string, Entity*> DataConverter::getPoIs() {
	const std::string entityString = cpi->GetFullPoIString();
	std::vector<std::string> entityElements = split(entityString, '|');
					
	for (std::vector<std::string>::iterator it = entityElements.begin(); it != entityElements.end(); ++it) {
		addEntity(*it);
	}

	return entities;
}

void DataConverter::addEntity(std::string entityParameters) {
	std::vector<std::string> entityElements = split(entityParameters, '~');
	
	if(entityElements.size() == 7) {
		std::string s = entityElements[3];
		std::replace(s.begin(),s.end(), ',', '.');
		std::string s2 = entityElements[4];
		std::replace(s2.begin(),s2.end(), ',', '.');

		double latMeters = EQUATOR_RADIUS * Asinh(tan(atof(s.c_str()) * (M_PI / 180)));
		double lonMeters = (atof(s2.c_str()) * (M_PI / 180)) * EQUATOR_RADIUS;

		Entity *ent = new Entity(entityElements[0], entityElements[1], entityElements[2], latMeters, 
			lonMeters, atof(entityElements[5].c_str()),atof(entityElements[6].c_str()));

		ent->entityString = entityParameters;

		entities.insert(std::pair<std::string, Entity*>(entityElements[0], ent));
	}
}

std::list<Entity*> DataConverter::getUpdatedEntities() {
	const std::string updateString = cpi->GetUpdateList();
	std::vector<std::string> entityElements = split(updateString, '|');
	updateObjectList.clear();
					
	for (std::vector<std::string>::iterator it = entityElements.begin(); it != entityElements.end(); ++it) {
		createUpdateEntityObject(*it);
	}

	return updateObjectList;
}

void DataConverter::createUpdateEntityObject(std::string entityParameters) {
	std::vector<std::string> entityElements = split(entityParameters, '~');

	if(entityElements.size() == 5) {
		std::string s = entityElements[1];
		std::replace(s.begin(),s.end(), ',', '.');
		std::string s2 = entityElements[2];
		std::replace(s2.begin(),s2.end(), ',', '.');

		double latMeters = EQUATOR_RADIUS * Asinh(tan(atof(s.c_str()) * (M_PI / 180)));
		double lonMeters = (atof(s2.c_str()) * (M_PI / 180)) * EQUATOR_RADIUS;

		/*Double3D pos;
		pos.x = atof(s.c_str());
		pos.y = atof(s2.c_str());
		pos.z = atof(entityElements[3].c_str());
		Double3D *tmp = extraMath->positionToLocalENU(pos,referencePoint);*/

		Entity *ent = new Entity(entityElements[0], "", "", latMeters, 
			lonMeters, atof(entityElements[3].c_str()), atof(entityElements[4].c_str()));
		
		updateObjectList.push_back(ent);
	}
}