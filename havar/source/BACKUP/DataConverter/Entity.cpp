#include "Entity.h"
#include <sstream>

Entity::Entity(std::string guid, std::string name, std::string type, double latitude, double longitude, double altitude, double orientation)
{
	this->guid		= guid;
	this->name		= name;
	this->type		= type;
	this->latitude	= latitude;
	this->longitude	= longitude;
	this->altitude	= altitude;
	this->orientation = orientation;
}


Entity::~Entity()
{
}
	
std::string Entity::getString() {
	return entityString;
}