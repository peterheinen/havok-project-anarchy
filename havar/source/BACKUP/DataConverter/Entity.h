#pragma once

#include <string>

class Entity
{
public:
	Entity(std::string guid, std::string name, std::string type, double latitude, double longitude, double altitude, double orientation);
	~Entity();

	std::string name, type, entityString, guid;
	double latitude, longitude, altitude, orientation;

	std::string getString();
};

