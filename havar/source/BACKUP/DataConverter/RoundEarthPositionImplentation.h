#pragma once
class RoundEarthPositionImplentation
{
public:
	RoundEarthPositionImplentation(double pX, double pY, double pZ);
	~RoundEarthPositionImplentation(void);

	double mX, mY, mZ, mLatitude, mLongitude;
	void updateLatLongAlt();
	double slantRangeTo(RoundEarthPositionImplentation *pPos);
	void updateXYZ();
};

