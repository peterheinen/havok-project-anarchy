#pragma once

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <list>

#import "C:\HavokARProject\Havar\Source\Havar\bin\Release\Havar.tlb" no_namespace named_guids
#import <mscorlib.tlb> raw_interfaces_only

#include "Entity.h"
#include "ExtraMath.h"

class DataConverter {
private:
    double EQUATOR_CIRCUMFERENCE;
	double EQUATOR_RADIUS;
public:
	DataConverter();
	~DataConverter();

	ExtraMath * extraMath;

	void init();
	int getNumber();
	std::vector<double> getTableClipBox();
	std::map<std::string, Entity*> getPoIs();
	bool isInitializedCorrectly;
	
	std::map<std::string, Entity*> entities;
	std::list<Entity*> updateObjectList;

	void addEntity(std::string);
	std::list<Entity*> getUpdatedEntities();
	void createUpdateEntityObject(std::string);

	IHavarInterface *cpi;

	double testDouble;
	Double3D referencePoint;
};