#include "RoundEarthPositionImplentation.h"

static double EARTH_RADIUS_AVG = 6371100.0; 

#define _USE_MATH_DEFINES
#include <math.h>

RoundEarthPositionImplentation::RoundEarthPositionImplentation(double pX, double pY, double pZ)
{
	mX = pX;
	mY = pY;
	mZ = pZ;
	updateLatLongAlt();
}


RoundEarthPositionImplentation::~RoundEarthPositionImplentation(void)
{
}

void RoundEarthPositionImplentation::updateLatLongAlt() {
	mLatitude = atan(mZ / sqrt((mX * mX) + (mY * mY)));
	double length = sqrt((mX * mX) + (mY * mY) + (mZ * mZ));

	double sinTheta = mY / (cos(mLatitude) * length);
	if(sinTheta > 0.999999999999f)
		sinTheta = 1;
	else if(sinTheta < -0.999999999999f)
		sinTheta = -1;

	double theta = asin(sinTheta);

	mLongitude = theta;

	if(mX < 0) {
		if(mY < 0) {
			mLongitude = -M_PI - theta;
		} else {
			mLongitude = M_PI - theta;
		}
	}

	mLatitude = length - EARTH_RADIUS_AVG;
}

double RoundEarthPositionImplentation::slantRangeTo(RoundEarthPositionImplentation* repi) {
	double dx = repi->mX - mX;
	double dy = repi->mY - mY;
	double dz = repi->mZ - mZ;
	return sqrt(dx * dx + dy * dy + dz * dz);
}

/*--radialen--

  public void setLatitude(double pLatitude) {
    mLatitude = pLatitude;
    mCosLatitude = Math.cos(mLatitude);
    mSinLatitude = Math.sin(mLatitude);
    updateXYZ();
  }

  public void setLongitude(double pLongitude) {
    mLongitude = pLongitude;
    mSinLongitude = Math.sin(mLongitude);
    mCosLongitude = Math.cos(mLongitude);
    updateXYZ();
  }
  
  /**
   * update xyz based on (changed) latitude, longitude and altitude
   * 
   * From geodetic round to geocentric round
   */
/*void RoundEarthPositionImplentation::updateXYZ() {
	mX = mCosLatitude * mCosLongitude * (EARTH_RADIUS_AVG + mAltitude);
    mY = mCosLatitude * mSinLongitude * (EARTH_RADIUS_AVG + mAltitude);
    mZ = mSinLatitude * (EARTH_RADIUS_AVG + mAltitude);
}*/