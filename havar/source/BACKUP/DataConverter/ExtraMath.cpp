#include "ExtraMath.h"
#include "RoundEarthPositionImplentation.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

static double EARTH_RADIUS_AVG = 6371100.0; 

ExtraMath::ExtraMath(void)
{
}


ExtraMath::~ExtraMath(void)
{
}

Double3D *ExtraMath::positionToLocalENU(Double3D pPos, Double3D refPos) {
	Double3D *result;

	currentRefPos = refPos;
	double groundRange = groundRangeTo(pPos);
	double heading = headingTo(pPos);
	result->x = sin(heading) * groundRange;
	result->y = cos(heading) * groundRange;
	result->z = pPos.z - refPos.z;
	return result;
}

double ExtraMath::groundRangeTo(Double3D pPos) {
	double x = pPos.x;
	double y = pPos.y;
	double z = pPos.z;

	double length1 = sqrt((currentRefPos.x * currentRefPos.x) + (currentRefPos.y * currentRefPos.y) + (currentRefPos.z * currentRefPos.z));
	double length2 = sqrt((x * x) + (y * y) + (z * z));

	double p1xp2 = (currentRefPos.x * x) + (currentRefPos.y * y) + (currentRefPos.z * z);
	double theta = aCos(p1xp2/(length1 * length2));
	
	if(theta == theta)
		return theta * EARTH_RADIUS_AVG;

	RoundEarthPositionImplentation *pos1 = new RoundEarthPositionImplentation(currentRefPos.x,currentRefPos.y, currentRefPos.z);
	RoundEarthPositionImplentation *pos2 = new RoundEarthPositionImplentation(x, y, z);

	return pos1->slantRangeTo(pos2);
}

double ExtraMath::headingTo(Double3D pPos) {
	double dlat = pPos.x - currentRefPos.x;
	double dlon = angleMinPIPlusPI(pPos.y - currentRefPos.y);
	double y = sin(dlat) + (1 - cos(dlon)) * sin(currentRefPos.y) * cos(pPos.y);
	double x = cos(pPos.y) * sin(dlon);

	return angleZero2PI(atan2(x,y));
}

double ExtraMath::angleMinPIPlusPI(double x) {
	return x - floor(x / (2 * M_PI)) * 2.0 * M_PI;
}

double ExtraMath::angleZero2PI(double pX) {
	double x = pX;
	if ((x <= - M_PI) || (x > M_PI)) {
		x = fmod(x, 2 * M_PI);
		if(x > M_PI) {
			x -= M_PI * 2;
		} 
		if(x <= -M_PI) {
			x += M_PI * 2;
		}
	}
	return x;
}

double ExtraMath::aCos(double a) {
	if ((a > 1) || (a < -1)) {
		return NULL;
	}

	if(a == 1) {
		return 0;
	}

	if(a == -1) {
		return M_PI;
	}

	double z = (1 - a) / (1 + a);
	return 2 * atan(sqrt(z));
}