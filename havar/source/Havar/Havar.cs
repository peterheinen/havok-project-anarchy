﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace Havar
{
    class Havar
    {
        // IMB Configuration
        private string mImbFollowApplication;
        private string mImbHostName;
        private int mImbPortNumber;
        private string mImbAlternativeHostName;
        private int mImbAlternativePortNumber;
        private int mImbClientId;
        private string mImbClientName;
        private string mImbFederation;

        private csImb.csImb mImbClient;
        private MapListener mMapListener;
        private PoIServiceManager mPoiServiceManager;

        const string TempFile = @"C:\Temp\IMBpluginLog.txt";

        public Havar()
        {

        }

        public bool Connect(String ip, int port)
        {
            // Delete the file if it exists. 
            if (File.Exists(TempFile))
            {
                File.Delete(TempFile);
            }

            //Create the file. 
            using (FileStream fs = File.Create(TempFile))
            {
                AddText(fs, "Beginning");

                // IMB Configuration
                mImbFollowApplication = "*";
                mImbHostName = ip;
                mImbPortNumber = port;
                mImbAlternativeHostName = "localhost";
                mImbAlternativePortNumber = 4000;
                mImbClientId = 34;
                mImbClientName = "havar";
                mImbFederation = "UST";

                Console.Out.WriteLine("--- IMB Configuration ---");
                Console.Out.WriteLine("FollowApplication: {0}", mImbFollowApplication);
                Console.Out.WriteLine("Host: {0}",              mImbHostName);
                Console.Out.WriteLine("Port: {0}",              mImbPortNumber);
                Console.Out.WriteLine("AlternativeHost: {0}",   mImbAlternativeHostName);
                Console.Out.WriteLine("AlternativePort: {0}",   mImbAlternativePortNumber);
                Console.Out.WriteLine("ClientId: {0}",          mImbClientId);
                Console.Out.WriteLine("ClientName: {0}",        mImbClientName);
                Console.Out.WriteLine("Federation: {0}",        mImbFederation);
                Console.Out.WriteLine("-------------------------");

                // -------------------- IMB --------------------
                // Create IMB instance
                mImbClient                  = new csImb.csImb() { Enabled = true };
                mImbClient.Connected        += ImbClient_Connected;
                mImbClient.Disconnected     += ImbClient_Disconnected;
                mImbClient.ClientAdded      += ImbClient_ClientAdded;
                mImbClient.ClientRemoved    += ImbClient_ClientRemoved;
                
                AddText(fs, "Creating new maplistener");
                mMapListener = new MapListener(mImbClient);
                AddText(fs, "Maplisterener created");

                AddText(fs, "Creating client status: " + mImbHostName + " " + ip);
                // Initialize IMB
                var clientStatus = new csImb.ImbClientStatus
                {
                    DisplayName = "Havar",
                    Client = true,
                    Type = "Service",
                    MyImage = "http://images-5.findicons.com/files/icons/1579/devine/48/globe.png",
                    Name = "Havar",
                    Application = "*",
                    Os = "Windows7",
                };

                AddText(fs, "Init: " + mImbHostName + " " + mImbPortNumber + " " + mImbClientName + " " + mImbClientId + " " + mImbFederation + " " + clientStatus + " " + mImbAlternativeHostName + " " + mImbAlternativePortNumber);
                mImbClient.Init(mImbHostName, mImbPortNumber, mImbClientName, mImbClientId, mImbFederation, clientStatus, mImbAlternativeHostName, mImbAlternativePortNumber);

                AddText(fs, "Trying to update to the client");
                // IMB connect
                mImbClient.UpdateStatus();
                
                if (mImbClient.Status.Id <= 0)
                {
                    AddText(fs, "FAILED!!! Environment will now exit!");

                    Console.Out.WriteLine("IMB: Unable to connect to server.");
                    Console.Out.WriteLine("IMB: Host = {0}, port = {1}, alternative host = {2}, alternative port = {3}", mImbHostName, mImbPortNumber, mImbAlternativeHostName, mImbAlternativePortNumber);
                    Environment.Exit(1);

                    return false;
                }

                AddText(fs, "Client update status correct");
                mPoiServiceManager = new PoIServiceManager(mImbClient);
                mPoiServiceManager.AddServiceToManage("*");
                mPoiServiceManager.Start();
                
                AddText(fs, "Everything went correct!");
            }

            return true;
        }

        public void CloseWindow()
        {
            mMapListener.StopAll();
            mImbClient.Close();
        }

        /// IMB connected
        void ImbClient_Connected(object sender, EventArgs e)
        {
            Console.Out.WriteLine("IMB: Connected to bus! IMB client status ID = {0}", mImbClient.Status.Id);
        }

        /// IMB disconnected
        void ImbClient_Disconnected(object sender, EventArgs e)
        {
            Console.Out.WriteLine("IMB: Disconnected from bus!");

            mMapListener.StopAll(); // Already called on program exit. ImbClient_Disconnected() is not called on connection loss.
            mPoiServiceManager.Stop();
        }   

        /// IMB client added
        void ImbClient_ClientAdded(object sender, csImb.ImbClientStatus e)
        {
            Console.Out.WriteLine("IMB: Client {0} added", e.Id); 
            mMapListener.Start(e);
        }

        /// IMB client removed
        void ImbClient_ClientRemoved(object sender, csImb.ImbClientStatus e)
        {
            Console.Out.WriteLine("IMB: Client {0} removed", e.Id);
        }

        public double[] getCameraPosition()
        {
            return mMapListener.getMapExtendCoords();
        }

        public string getFullPoIString()
        {
            return mPoiServiceManager.getFullPoIString();
        }

        public string GetUpdateList() 
        {
            return mPoiServiceManager.GetUpdateList();
        }

        public string GetCameraData()
        {
            return mMapListener.getCameraData();
        }

        public bool ismapExtendChanged()
        {
            return mMapListener.ismapExtendChanged();
        }
        
        public bool IsCameraDataChanged()
        {
            return mMapListener.IsCameraDataChanged();
        }

        public bool isPoIChanged()
        {
            return mPoiServiceManager.isPoiChanged();
        }

        internal bool shouldUpdate()
        {
            return mPoiServiceManager.shouldUpdate();
        }

        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
            byte[] newline = Encoding.ASCII.GetBytes(Environment.NewLine);
            fs.Write(newline, 0, newline.Length);
        }
    }
}
