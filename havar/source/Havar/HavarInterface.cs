﻿using System;
using System.Runtime.InteropServices;
using Havar;

namespace Havar
{
    [Guid("DBE0E8C4-1C61-41f3-B6A4-4E2F353D3D05")]
    public interface IHavarInterface
    {
        void        ShutDownHavar();
        double[]    GetCameraPosition();
        String      GetFullPoIString();
        String      GetUpdateList();
        String      GetCameraData();

        bool InitHavar(String ip, int port);
        bool IsmapExtendChanged();
        bool IsPoIChanged();
        bool ShouldUpdate();
        bool IsCameraDataChanged();
    }

    [Guid("C6659361-1625-4746-931C-36014B146679")]
    public class InterfaceImplementation : IHavarInterface
    {
        private Havar hav;

        // returns whether a connection could be set up with the IMB bus.
        public bool InitHavar(String ip, int port)
        {
            hav = new Havar();

            if (hav.Connect(ip, port))
                return true;
            else
            {
                hav = null;
                return false;
            }
        }

        public void ShutDownHavar()
        {
            hav.CloseWindow();
            hav = null;
        }

        public String GetFullPoIString()
        {
            return hav.getFullPoIString();
        }

        public String GetUpdateList()
        {
            return hav.GetUpdateList();
        }

        public String GetCameraData()
        {
            return hav.GetCameraData();
        }

        public double[] GetCameraPosition()
        {
            return hav.getCameraPosition();
        }

        public bool IsmapExtendChanged()
        {
            return hav.ismapExtendChanged();
        }

        public bool IsPoIChanged()
        {
            return hav.isPoIChanged();
        }

        public bool IsCameraDataChanged()
        {
            return hav.IsCameraDataChanged();
        }

        public bool ShouldUpdate()
        {
            return hav.shouldUpdate();
        }
    }
}