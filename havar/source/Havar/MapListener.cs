﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havar
{
    class MapListener
    {
        private string eventName            = "fastmapextent";
        private string cameraEventName      = "3d";
        private string cameraData           = "";

        private double[]                    mMapExtendCoords;
        private csImb.csImb                 mImbClient;
        private List<csImb.ImbClientStatus> mClients = new List<csImb.ImbClientStatus>();           
        private List<IMB3.TEventEntry>      mMapExtentEventsCollection = new List<IMB3.TEventEntry>();   // Mirrored list

        private bool mapExtendChanged   = false;
        private bool cameraDataChanged  = false;

        /// Constructor
        public MapListener(csImb.csImb imbClient)
        {
            mImbClient = imbClient;

            mMapExtendCoords = new double[4];

            for(int i = 0; i < 4; i++)
                mMapExtendCoords[i] = i;
        }

        /// <summary>
        /// Start listening to the map of this client. Subscribe for 'fastmapextent' events.
        /// </summary>
        /// <param name="client">The client to start listening to</param>
        public void Start(csImb.ImbClientStatus client)
        {
            Console.Out.WriteLine("MapListener: Subscribing to {0}.{1}", client.Id, eventName);

            // Subscribe for Map Extent events
            IMB3.TEventEntry camChangedEvents   = mImbClient.Imb.Subscribe(client.Id + "." + cameraEventName);
            IMB3.TEventEntry mapExtentEvents    = mImbClient.Imb.Subscribe(client.Id + "." + eventName);
            camChangedEvents.OnNormalEvent  += IMBCameraEvent;
            mapExtentEvents.OnNormalEvent   += ImbMapExtentEvent;

            // Add client and map extent event entry to list
            mClients.Add(client);
            mMapExtentEventsCollection.Add(mapExtentEvents);
        }

        /// <summary>
        /// Stop listening to the map of this client. Unsubscribe for 'fastmapextent' events.
        /// </summary>
        /// <param name="client">The client to stop listening to</param>
        public void Stop(csImb.ImbClientStatus client)
        {
            Console.Out.WriteLine("MapListener: Unsubscribing from {0}.{1}", client.Id, eventName);

            int index = UnsubscribeClient(client);
            if (index >= 0)
            {
                // Remove client from list
                mClients.RemoveAt(index);
                mMapExtentEventsCollection.RemoveAt(index);
            }
        }

        /// <summary>
        /// Stop listening to the maps of all clients. Unsubscribe for 'fastmapextent' events.
        /// </summary>
        public void StopAll()
        {
            foreach (csImb.ImbClientStatus client in mClients)
            {
                Console.Out.WriteLine("MapListener: Unsubscribing from {0}.{1}", client.Id, eventName);
                UnsubscribeClient(client);
            }

            mClients.Clear();
            mMapExtentEventsCollection.Clear();
        }

        private int UnsubscribeClient(csImb.ImbClientStatus client)
        {
            int index = mClients.IndexOf(client);
            if (index >= 0)
            {
                IMB3.TEventEntry mapExtentEvents = mMapExtentEventsCollection.ElementAt<IMB3.TEventEntry>(index);
                mapExtentEvents.OnNormalEvent -= ImbMapExtentEvent;
            }

            // Unsubscribe from Map Extent events
            mImbClient.Imb.UnSubscribe(client.Id + "." + eventName);

            return index;
        }

        /// Map extent event occured
        private void ImbMapExtentEvent(IMB3.TEventEntry aEvent, IMB3.ByteBuffers.TByteBuffer aPayload)
        {
            string payloadStr = aPayload.ReadString();

            // Split payload (get coordinates)
            char[] payloadDelimiters = { '|' };
            string[] coordinateStrs = payloadStr.Split(payloadDelimiters);

            mMapExtendCoords = new double[4];
            
            double[] coordinates;

            if (ParseStringsToDoubles(coordinateStrs, out coordinates))
            {
                mMapExtendCoords = coordinates;
                mapExtendChanged = true;
            }
        }

        /// Map extent event occured
        private void IMBCameraEvent(IMB3.TEventEntry aEvent, IMB3.ByteBuffers.TByteBuffer aPayload)
        {
            cameraDataChanged = true;
            cameraData = aPayload.ReadString();
        }

        /// <summary>
        /// Returns the map extend coordinates to the Interface so it can be transferred over the COM.
        /// </summary>
        public double[] getMapExtendCoords()
        {
            mapExtendChanged = false;
            return mMapExtendCoords;
        }

        public string getCameraData()
        {
            cameraDataChanged = false;
            return cameraData;
        }

        /// <summary>
        /// Returns whether the map extend coordinates have changed, so it should update the camera position.
        /// </summary>
        /// <returns></returns>
        public bool ismapExtendChanged()
        {
            return mapExtendChanged;
        }

        /// <summary>
        /// Returns whether the map extend coordinates have changed, so it should update the camera position.
        /// </summary>
        /// <returns></returns>
        public bool IsCameraDataChanged()
        {
            return cameraDataChanged;
        }

        /// <summary>
        /// Tries to parse a string to a double.
        /// If it succeeds it returns true, else it returns false.
        /// </summary>
        public static bool ParseStringsToDoubles(string[] strs, out double[] doubles)
        {
            doubles = new double[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                if (!Double.TryParse(strs[i], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out doubles[i]))
                    return false;
            }
            return true;
        }
    }
}
