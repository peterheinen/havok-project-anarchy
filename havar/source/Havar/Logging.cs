﻿using System;
using System.IO;
using System.Text;

public class Logger
{
    const string TempFolder = @"C:\Temp";
    const string TempFile = @"C:\Temp\IMBpluginLog.txt";
    private static Logger instance;

    private Logger()
    {
        if (!Directory.Exists(TempFolder)) Directory.CreateDirectory(TempFolder);
        File.Create(TempFile);
    }

    public static Logger Instance
    {
        get { return instance ?? (instance = new Logger()); }
    }

    public string LogFile { get { return Path.Combine(TempFolder, TempFile); } }

    public bool IsLogging { get; set; }

    public void WriteInfo(string message)
    {
        using (var sw = File.OpenWrite(LogFile)) {
            byte[] myMessage = new UTF8Encoding(true).GetBytes(message);
            sw.Write(myMessage, 0, myMessage.Length); 
        }
    }

    public void WriteWarning(string message)
    {
        if (!IsLogging) return;
    }

    public void WriteError(string message)
    {
        if (!IsLogging) return;
    }
}

