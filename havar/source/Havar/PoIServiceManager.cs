﻿using DataServer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havar
{
    class PoIServiceManager
    {
        private DataServerBase  mDsb;
        private List<String>    mServiceNamesToManage = new List<String>();
        private List<PoI>       PoIs = new List<PoI>();
        private List<PoI>       updateList = new List<PoI>();
        private bool poiChanged = false;
        private bool mustUpdate = false;
        
        // Mutex
        private static object mPoisLock = new object(); 

        public PoIServiceManager(csImb.csImb imbClient)
        {
            mDsb = new DataServer.DataServerBase() { SyncPriority = 5 };
            mDsb.client = imbClient;
            mDsb.AutoTracking = true;
            mDsb.Services.CollectionChanged += Services_CollectionChanged;
            mDsb.Subscribed += dsb_Subscribed;
            mDsb.UnSubscribed += dsb_UnSubscribed;
        }

        /// <summary>
        /// Start listening to new PoI services.
        /// </summary>
        public void Start()
        {
            mDsb.Start("", Mode.client, autoTrack: false);
        }

        /// <summary>
        /// Stop managing all services and stop listening to new ones.
        /// </summary>
        public void Stop()
        {
            // For all services
            foreach (Service service in mDsb.Services)
            {
                // Unsubscribe from service
                StopManagingService(service);

                // Disconnect PoI event handlers
                PoiService poiService = service as PoiService;
                if (poiService != null)
                {
                    DetachPoiEventHandlers(poiService);
                }
            }
            mDsb.Stop();
        }

        /// <summary>
        /// Add a PoI service to manage.
        /// </summary>
        /// <param name="serviceName">The name of the service</param>
        public void AddServiceToManage(String serviceName)
        {
            mServiceNamesToManage.Add(serviceName.ToLower());
        }


        /* ******************** Internal ******************** */
        // Subscribe to service
        private void StartManagingService(Service service)
        {
            service.Initialized += service_Initialized;
            mDsb.Subscribe(service, Mode.client);
        }

        // UnSubscribe from service
        private void StopManagingService(Service service)
        {
            if (service.IsSubscribed)   // True on program exit. False on service stopped. 
            {
                service.Unsubscribe();
                mDsb.UnSubscribe(service);
            }
            service.Initialized -= service_Initialized;
        }

        // Attach PoI event handlers
        private void AttachPoiEventHandlers(PoiService poiService)
        {
            foreach (PoI poi in poiService.PoIs)
            {
                PoIs.Add(poi);
                poi.PositionChanged += PositionChanged;
            }

            poiChanged = true;
        }

        // Detach PoI event handlers
        private void DetachPoiEventHandlers(PoiService poiService)
        {

        }

        /* **************************************************
        * PoI Data Services
        * **************************************************/

        /// Services collection changed
        private void Services_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            try
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (Service service in e.NewItems)
                        {
                            Console.Out.WriteLine("PoiServicesManager: New service {0} available.", service.Id);
                            Console.Out.WriteLine("- Name: {0}", service.Name);
                            Console.Out.WriteLine("- Online status: {0}", service.OnlineStatus);

                            PoiService poiService = service as PoiService;
                            if (poiService != null)
                            {
                                if (mServiceNamesToManage.Contains(poiService.Name.ToLower()) || mServiceNamesToManage.Contains("*"))
                                {
                                    StartManagingService(poiService);
                                }
                            }
                        }
                        break;
                    case NotifyCollectionChangedAction.Remove:  // Note: The service is already unsubscribed before this code is reached
                        foreach (Service service in e.OldItems)
                        {
                            PoiService poiService = service as PoiService;
                            if (poiService != null)
                            {
                                StopManagingService(poiService);
                                Console.Out.WriteLine("PoiServicesManager: Removed {0} service.", service.Id);
                            }
                        }
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        // Remove all services
                        foreach (Service service in mDsb.Services)
                        {
                            PoiService poiService = service as PoiService;
                            if (poiService != null)
                            {
                                StopManagingService(poiService);
                                Console.Out.WriteLine("PoiServicesManager: Removed {0} service.", service.Id);
                            }
                        }
                        break;
                }
            }
            catch (Exception execption)
            {
                 Console.Out.WriteLine(execption.Message);
            }
        }

        /// Subscribed to service
        private void dsb_Subscribed(object sender, DataServer.ServiceSubscribeEventArgs e)
        {
            Console.Out.WriteLine("PoiServicesManager: Subscribed to service {0}", e.Service.Id);
        }

        /// UnSubscribed from service
        private void dsb_UnSubscribed(object sender, ServiceSubscribeEventArgs e)
        {
            Console.Out.WriteLine("PoiServicesManager: UnSubscribed from service {0}", e.Service.Id);

            PoiService poiService = e.Service as PoiService;
            if (poiService != null)
                DetachPoiEventHandlers(poiService);
        }

        /// PoI Service initialized
        private void service_Initialized(object sender, EventArgs e)
        {
            PoiService poiService = (PoiService)sender;
            Console.Out.WriteLine("PoiServicesManager: PoI Service {0} initialized.", poiService.Id);
            AttachPoiEventHandlers(poiService);
        }

        public String getFullPoIString()
        {
            poiChanged = false;

            if (PoIs.Count() == 0)
            {
                return "PoIs list is still empty";
            }
            else
            {
                var sb = new StringBuilder();
                var extras = new StringBuilder();

                foreach (PoI poi in PoIs)
                {
                    string model, lat, lon, alt, orient, move, isActive, range = "", vertAngle, horAngle;
                    poi.Labels.TryGetValue("TimeBased.Model", out model);
                    poi.Labels.TryGetValue("TimeBased.Lat", out lat);
                    poi.Labels.TryGetValue("TimeBased.Lon", out lon);
                    poi.Labels.TryGetValue("TimeBased.Alt", out alt);
                    poi.Labels.TryGetValue("TimeBased.Orient", out orient);
                    poi.Labels.TryGetValue("TimeBased.Move", out move);
                    poi.Labels.TryGetValue("IsActive", out isActive);
                    poi.Labels.TryGetValue("Range", out range);
                    poi.Labels.TryGetValue("ViewAngle", out horAngle);
                    poi.Labels.TryGetValue("ViewAngleVertical", out vertAngle);

                    if (range != null)
                    {
                        extras.Append(range + "&" + horAngle + "&" + vertAngle);
                    }

                    if (poi.Labels.ContainsKey("TimeBased.Lat") && poi.Labels.ContainsKey("TimeBased.Lon"))
                    {
                        Console.Out.WriteLine("Animation information {0} {1} {2} {3} {4} {5} {6} {7} {8} ", poi.Id, poi.Name, model, lat, lon, orient, move, isActive, PoIs.Count, range, vertAngle, horAngle);
                        sb.Append(poi.Id + "~" + poi.Name + "~" + model + "~" + lat + "~" + lon + "~" + alt + "~" + orient + "~" + move + "~" + isActive + "|");
                    }
                    else
                    {
                        sb.Append(poi.Id + "~" + poi.Name + "~" + poi.ContentId + "~" + poi.Position.Latitude + "~" + poi.Position.Longitude + "~" + poi.Position.Altitude + "~" + poi.Orientation + "|");
                    }
                }

                return sb.ToString();
            }
        }

        public String GetUpdateList()
        {
            lock (mPoisLock)
            {
                mustUpdate = false;

                var sb = new StringBuilder();
                var extras = new StringBuilder();

                foreach (PoI poi in PoIs)
                {
                    string model, lat, lon, alt, orient, move, isActive, range = "", vertAngle, horAngle;
                    poi.Labels.TryGetValue("TimeBased.Model", out model);
                    poi.Labels.TryGetValue("TimeBased.Lat", out lat);
                    poi.Labels.TryGetValue("TimeBased.Lon", out lon);
                    poi.Labels.TryGetValue("TimeBased.Alt", out alt);
                    poi.Labels.TryGetValue("TimeBased.Orient", out orient);
                    poi.Labels.TryGetValue("TimeBased.Move", out move);
                    poi.Labels.TryGetValue("IsActive", out isActive);
                    poi.Labels.TryGetValue("Range", out range);
                    poi.Labels.TryGetValue("ViewAngle", out horAngle);
                    poi.Labels.TryGetValue("ViewAngleVertical", out vertAngle);

                    if(range != null) {
                        extras.Append(range + "&" + horAngle + "&" + vertAngle);
                    }
                    Console.Out.WriteLine("Animation information {0} {1} {2} {3} {4} {5} {6} {7} {8} {9}", poi.Id, poi.Name, model, lat, lon, orient, move, isActive, PoIs.Count, extras.ToString());

                    if (poi.Labels.ContainsKey("TimeBased.Lat") && poi.Labels.ContainsKey("TimeBased.Lon"))
                    {
                        sb.Append(poi.Id + "~" + poi.Name + "~" + model + "~" + lat + "~" + lon + "~" + alt + "~" + orient + "~" + move + "~" + isActive + "|");
                    }
                    else
                    {
                        sb.Append(poi.Id + "~" + poi.Name + "~" + poi.ContentId + "~" + poi.Position.Latitude + "~" + poi.Position.Longitude + "~" + poi.Position.Altitude + "~" + poi.Orientation + "|");
                    }
                }

                updateList.Clear();

                return sb.ToString();
            }
        }

        public bool isPoiChanged()
        {
            return poiChanged;
        }

        public bool shouldUpdate()
        {
            return mustUpdate;
        }

        public void PositionChanged(object sender, PositionEventArgs e)
        {
            lock(mPoisLock) {
                mustUpdate = true;
                PoI poi = (PoI)sender;
                updateList.Add(poi);
            }
        }
    }
}
