﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havar
{
    class Starter
    {
        static int Main(string[] args)
        {
            Havar hav = new Havar();
            hav.Connect("localhost",4000);

            while (true)
            {
                if (hav.shouldUpdate())
                {
                    hav.GetUpdateList();
                }
            }
        }
    }
}
