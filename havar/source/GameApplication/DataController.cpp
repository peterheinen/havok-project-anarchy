#include "GameApplicationPCH.h"
#include "DataController.h"

DataController::DataController(ModelMain *pModelMain)
{
	mIsInitialized	= false;
	mUtil			= new Util();
	mModelMain		= pModelMain;
	mDataConverter	= new DataConverter();
	
	mModelMain->mEntities->scaling			= 0.01;
	mModelMain->mEntities->scaleThreshold	= 0.01;
	connected = 0;
	ipAdressConnect = "";
	myPort = 0;
}


DataController::~DataController()
{

}

void DataController::Init(std::string ipAddres, int port) 
{
	myPort = port;
	ipAdressConnect = ipAddres;
	connected = 1;

	if(mDataConverter->isInitializedCorrectly) {
		mIsInitialized = true;
	} else {
		mDataConverter->Init(ipAddres, port);
	}
}

void DataController::Update() 
{
	if(mIsInitialized) {
		if(mDataConverter->cpi->IsPoIChanged()) {
			std::map<std::string, Entity*> entities = mDataConverter->getPoIs();
			
			for(std::map<std::string, Entity*>::iterator ii = entities.begin(); ii != entities.end(); ++ii ) {
				if(mModelMain->mEntities->gameObjects.find((*ii).second->guid) == mModelMain->mEntities->gameObjects.end()) {
					VisBaseEntity_cl* tmpObject;
					std::string modelName = "";
			
					hkvVec3d convertedLatLongAltCoords	= mUtil->ConvertLatLongAltToHavokCoords((*ii).second->latitude, (*ii).second->longitude, (*ii).second->altitude);
					hkvVec3 objectPosition				= hkvVec3(convertedLatLongAltCoords.x + mModelMain->mEntities->xOffset, convertedLatLongAltCoords.y + mModelMain->mEntities->yOffset, convertedLatLongAltCoords.z);

					if((*ii).second->type == "Man") {
						modelName.append("Models\\Misc\\Eddy.MODEL" );					
					} else if((*ii).second->type == "Woman" || (*ii).second->name == "Woman") {
						modelName.append("Models\\Misc\\female_008_high.model" );
					} else if((*ii).second->type == "Soldier") {
						modelName.append("Models\\Misc\\soldier_high.MODEL" );
					} else if((*ii).second->type == "Helicopter") {
						modelName.append("Models\\Misc\\ah-64-armed.model" );
					} else if((*ii).second->type == "Child" || (*ii).second->name == "Woman") {
						modelName.append("Models\\Misc\\female_008_high.model" );
					} else if((*ii).second->type == "Aeroplane") {
						modelName.append("Models\\Misc\\boeing_737.model" );
					} else if((*ii).second->type == "Tank") {
						modelName.append("Models\\Misc\\leopard2a6.model" );
					} else if((*ii).second->type == "StaticWeapon") {
						modelName.append("Models\\Misc\\sentinel.model" );
					} else if((*ii).second->type == "Jeep") {
						modelName.append("Models\\Misc\\fennek_des.model" );
					} else if((*ii).second->name == "Havok Sensor") {
						modelName.append("Models\\Misc\\Range.model" );
					}

					if(modelName != "") {
						tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", objectPosition, modelName.c_str());
						tmpObject->SetOrientation((*ii).second->orientation - 90, 0,0);
						tmpObject->SetScaling(mModelMain->mEntities->scaling);
						mModelMain->mEntities->gameObjects.insert(std::pair<std::string, VisBaseEntity_cl*>((*ii).second->guid, tmpObject));

						if((*ii).second->animation != "") {
							VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
								VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, (*ii).second->animation.c_str(), VANIMCTRL_LOOP);
						} else {
							VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
								VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, "Idle", VANIMCTRL_LOOP);
							
						}
						
						if((*ii).second->name == "Havok Sensor") {
							tmpObject->SetScaling(hkvVec3(0.05f, 0.1f, 0.05f));
						}
					} else {
						mModelMain->mEntities->myTypes.append(" " + (*ii).second->name + " & " + (*ii).second->type + "|");
						modelName.append("Models\\Misc\\" + (*ii).second->type + ".model");
						
						tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", objectPosition, modelName.c_str());
						tmpObject->SetOrientation((*ii).second->orientation - 90, 0,0);
						tmpObject->SetScaling(0.01);
						mModelMain->mEntities->gameObjects.insert(std::pair<std::string, VisBaseEntity_cl*>((*ii).second->guid, tmpObject));

						std::string str2 ("Step");
						std::size_t found = (*ii).second->name.find(str2);
					
						if(found != std::string::npos)
							tmpObject->SetScaling(0.00);

						if((*ii).second->animation != "") {
							std::string str2 ("Idle");
							std::size_t found = (*ii).second->animation.find(str2);
					
							if(found != std::string::npos) {
								VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
									VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, "Idle_Breath", VANIMCTRL_LOOP);
							} else {
								VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
									VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, (*ii).second->animation.c_str(), VANIMCTRL_LOOP);
							}
						} else {
							VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl =
								VisAnimConfig_cl::StartSkeletalAnimation(tmpObject, "Idle", VANIMCTRL_LOOP);
						}
					}

					if((*ii).second->type == "StaticWeapon") {				
						VisBaseEntity_cl* rangeObject = Vision::Game.CreateEntity("VisBaseEntity_cl", objectPosition, "Models\\Misc\\dome.model");		
						rangeObject->SetScaling(3.5);
					
						rangeObject->AttachToParent(tmpObject);
						mModelMain->mEntities->rangeObjects.push_back(rangeObject);
					}
				}
			}
		}

		if(mDataConverter->cpi->ShouldUpdate()) {
			std::list<Entity*> updateObjectList = mDataConverter->getUpdatedEntities();

			for(std::list<Entity*>::iterator poiIterator = updateObjectList.begin(); poiIterator != updateObjectList.end(); ++poiIterator ) {
				for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->gameObjects.begin(); ii != mModelMain->mEntities->gameObjects.end(); ++ii ) {
					if((*ii).first == ((*poiIterator)->guid)) {
						hkvVec3d convertedLatLongAltCoords	= mUtil->ConvertLatLongAltToHavokCoords((*poiIterator)->latitude, (*poiIterator)->longitude, (*poiIterator)->altitude);
						hkvVec3 objectPosition				= hkvVec3(convertedLatLongAltCoords.x + mModelMain->mEntities->xOffset, convertedLatLongAltCoords.y + mModelMain->mEntities->yOffset, convertedLatLongAltCoords.z);

						(*ii).second->SetPosition(objectPosition);
						(*ii).second->SetOrientation(-(*poiIterator)->orientation, 0, 0);

						if(!(*poiIterator)->isActive) {
							(*ii).second->SetScaling(0);
						} else if((*ii).second->GetScaling() == hkvVec3(0)) {
							(*ii).second->SetScaling(mModelMain->mEntities->scaling);
						}

						std::string str2 ("Step");
						std::size_t found = (*poiIterator)->name.find(str2);
					
						if(found != std::string::npos)
							(*ii).second->SetScaling(0.00);

						if((*poiIterator)->animation != "") {
							VSmartPtr<VisSkeletalAnimControl_cl> spAnimControl = 
								VisAnimConfig_cl::StartSkeletalAnimation((*ii).second, (*poiIterator)->animation.c_str(), VANIMCTRL_LOOP);
						}
					}
				}		
			}
		}

		if(mDataConverter->cpi->IsmapExtendChanged()) {
			std::vector<double> clipBox(4);
			clipBox	= mDataConverter->getTableClipBox();

			hkvVec3d topLeft		= mUtil->ConvertMetersToHavokCoords(clipBox[1], clipBox[0]);
			hkvVec3d bottomRight	= mUtil->ConvertMetersToHavokCoords(clipBox[3], clipBox[2]);

			double realWidthDistance	= bottomRight.x - topLeft.x;
			double realHeightDistance	= bottomRight.y - topLeft.y;

			double xTopLeft		= topLeft.x;
			double yTopLeft		= topLeft.y;
			double xBottomRight	= bottomRight.x;
			double yBottomRight = bottomRight.y;

			double xOffset = (xTopLeft + xBottomRight) / 2;
			double yOffset = (yBottomRight + yTopLeft) / 2;

			mModelMain->mMap->clipBox				= clipBox;
			mModelMain->mMap->topLeft				= topLeft;
			mModelMain->mMap->bottomRight			= bottomRight;
			mModelMain->mMap->realHeightDistance	= realHeightDistance;
			mModelMain->mMap->realWidthDistance		= realWidthDistance;
			mModelMain->mMap->xOffset				= xOffset;
			mModelMain->mMap->yOffset				= yOffset;
		}

		if(mDataConverter->cpi->IsCameraDataChanged()) { 
			std::vector<double> cameraData(6);
			cameraData = mDataConverter->getCameraData();

			hkvMat4 havokMatrix;
			hkvVec3d camPosD		=	mUtil->ConvertLatLongAltToHavokCoords(cameraData[1], cameraData[0], cameraData[2]);
			hkvVec3d targetPosD		=	mUtil->ConvertLatLongAltToHavokCoords(cameraData[4], cameraData[3], cameraData[5]);
			hkvVec3 camPos(camPosD.x,camPosD.y,camPosD.z);
			hkvVec3 targetPos(targetPosD.x,targetPosD.y,targetPosD.z);

			havokMatrix.setLookAtMatrix(camPos, targetPos);
			
			float yaw, pitch, roll;
			havokMatrix.getAsEulerAngles(roll,pitch,yaw);
			hkvVec3 position = havokMatrix.getTranslation();
			
			mModelMain->mCamera->externalCameraPosition = new hkvVec3(position.x,position.y,position.z);
			mModelMain->mCamera->externalCameraOrientation = new hkvVec3(yaw,pitch,roll);
		}
	}
}