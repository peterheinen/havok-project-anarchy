#pragma once

#include "MenuController.h"
#include "ModelMain.h"
#include "DataController.h"

class InputController
{
public:
	InputController(ModelMain *pModelMain, MenuController *pMenuController, DataController *pDataController);
	~InputController(void);

	MenuController	*mMenuController;
	ModelMain		*mModelMain;
	DataController	*mDataController;
	bool shouldAR, external;

	void Update();
	void ChangeModelPositionSlightly(float x, float y);
	void ChangeScaling(float scale);
};

