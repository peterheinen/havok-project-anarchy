#pragma once

#include "ARCamera.h"
#include "ModelMain.h"

class CameraController
{
public:
	CameraController(ModelMain *pModelMain);
	~CameraController(void);

	void Update();
	void ChangeState();

	ModelMain	*mModelMain;
	ARCamera	*mARCamera;
};

