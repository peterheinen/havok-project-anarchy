#pragma once

#include "CameraController.h"
#include "DataController.h"
#include "InputController.h"
#include "MenuController.h"
#include "GUI.h"
#include "ModelMain.h"

class ControllerManager
{
public:
	ControllerManager(ModelMain *pModelMain);
	~ControllerManager(void);

	ModelMain			*mModelMain;
	CameraController	*mCameraController;
	MenuController		*mMenuController;
	InputController		*mInputController;
	DataController		*mDataController;

	void Init(GUI *mGui);
	void Update();
};

