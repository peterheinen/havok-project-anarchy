#pragma once
class HavokCamera
{
public:
	HavokCamera(void);
	~HavokCamera(void);

	VisBaseEntity_cl	*mCamera; 
	VisMouseCamera_cl	*mMouseCamera;

	hkvVec3	*externalCameraPosition;
	hkvVec3 *externalCameraOrientation;

	float mMoveSpeed, mSensitivity, mUpDownSpeed;

	void SetCamera(VisBaseEntity_cl	*pCamera);
	void DisableInput();
	void EnableInput();
	void PrintDebug();
};

