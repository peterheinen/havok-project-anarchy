#pragma once

#include "ModelMain.h"

class ARCamera
{
public:
	ARCamera(ModelMain *pModelMain);
	~ARCamera(void);

	int videoXRes;
	int videoYRes; 

	bool mInit, invertImage, mMarkerInit;

	VisBaseEntity_cl	*mCameraCorrector;
	ModelMain			*mModelMain;

	double mTableWidth;   
	double mTableHeight;   
	
	void Init();
	void Update();
};