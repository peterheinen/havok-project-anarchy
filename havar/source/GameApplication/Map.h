#pragma once

#include <string>
#include <iostream>
#include <stdlib.h>
#include <vector>

class Map
{
public:
	Map(void);
	~Map(void);

	std::vector<double> clipBox;		
	VisBaseEntity_cl *pCamera; 

	hkvVec3d topLeft;
	hkvVec3d bottomRight;
	double xOffset, manualXOffset, yOffset, manualYOffset, mTableWidth, mTableHeight, realHeightDistance, realWidthDistance;

	void PrintDebug();
};