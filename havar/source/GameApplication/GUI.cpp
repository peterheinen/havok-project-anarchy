#include "GameApplicationPCH.h"
#include "GUI.h"

GUI::GUI(void)
{
}


GUI::~GUI(void)
{
}


void GUI::InitGUI()
{
	spGUIContext = new VGUIMainContext(NULL);
	spGUIContext->SetActivate(true);
	spGUIContext->SetShowCursor(false);

	// Load some default resources (like fonts or the image for the cursor)
	VGUIManager::GlobalManager().LoadResourceFile("Dialogs\\MenuSystem.xml");

	// Load and show the Main Menu layout
	spMainDlg = spGUIContext->ShowDialog("Dialogs\\MainMenu.xml");
	spMainDlg->SetVisible(false);

	VASSERT(spMainDlg);
}

void GUI::DeInitGUI()
{
  spMainDlg = NULL;                  // destroy the Main Dialog object
  spGUIContext->SetActivate(false);  // deactivate the GUI context ...
  spGUIContext = NULL;               // ... and destroy it
}

void GUI::ProcessGUIResult() {
	int dialogResult = spMainDlg->GetDialogResult();
	
	if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWRANGES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX1"));
		VASSERT(checkbox);
		//showRanges = checkbox->IsChecked();
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWDROPLINES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX2"));
		VASSERT(checkbox);
		//showAirCorridors = checkbox->IsChecked();
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SCALE")) {
		VSliderControl *slider = (VSliderControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("WALKSTAND_SLIDER"));
		VASSERT(slider);
		//scaling = slider->GetValue() / 100;
	}	
	
	/*if(Vision::Key.IsPressed(VGLK_P)) {
		spMainDlg->SetVisible(true);
	} else if(Vision::Key.IsPressed(VGLK_O) || (dialogResult == VGUIManager::GlobalManager().GetID("HIDEGUI"))) {
		spMainDlg->SetVisible(false);
		spMainDlg->SetDialogResult(VGUIManager::ID_OK);
	}*/
}