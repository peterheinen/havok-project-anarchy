#pragma once

#include <string>
#include <iostream>
#include <stdlib.h>
#include <list>
#include <map>
#include "DataConverter.h"

class Entities
{
public:
	Entities(void);
	~Entities(void);

	bool mShowDropLines, isActive;
	float xOffset, yOffset;
	double scaling, scaleThreshold;

	std::list<std::string>						types;
	std::string									myTypes;
	std::map<std::string, VisBaseEntity_cl*>	gameObjects;
	std::list<VisBaseEntity_cl*>				rangeObjects;
	std::list<VisBaseEntity_cl*>				airCorridors;

	void PrintDebug();
};

