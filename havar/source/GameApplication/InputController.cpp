#include "GameApplicationPCH.h"
#include "InputController.h"


InputController::InputController(ModelMain *pModelMain, MenuController *pMenuController, DataController *pDataController)
{
	mMenuController = pMenuController;
	mModelMain		= pModelMain;
	mDataController = pDataController;

	shouldAR = false;
	external = false;
}

InputController::~InputController(void)
{
}

void InputController::Update() {
	if(!mMenuController->spMainDlg->IsVisible()) {
		if(Vision::Key.IsPressed(VGLK_O)) {
			mModelMain->mCamera->DisableInput();
			mMenuController->spMainDlg->SetVisible(true);
			mMenuController->spGUIContext->SetShowCursor(true);
		} else if (Vision::Key.IsPressed(VGLK_A)) { mModelMain->PrintDebug();
		} else if (Vision::Key.IsPressed(VGLK_Q)) { mModelMain->mMap->manualXOffset -= 0.25f;
		} else if (Vision::Key.IsPressed(VGLK_W)) { mModelMain->mMap->manualXOffset += 0.25f;
		} else if (Vision::Key.IsPressed(VGLK_E)) { mModelMain->mMap->manualYOffset -= 0.25f;
		} else if (Vision::Key.IsPressed(VGLK_R)) { mModelMain->mMap->manualYOffset += 0.25f;
		} else if (Vision::Key.IsPressed(VGLK_Z)) { ChangeModelPositionSlightly(0.1f,  0);
		} else if (Vision::Key.IsPressed(VGLK_X)) { ChangeModelPositionSlightly(-0.1f, 0);
		} else if (Vision::Key.IsPressed(VGLK_C)) { ChangeModelPositionSlightly(0, 0.1f);
		} else if (Vision::Key.IsPressed(VGLK_V)) { ChangeModelPositionSlightly(0, -0.1f);
		} else if (Vision::Key.IsPressed(VGLK_K)) { ChangeScaling(-0.01); 
		} else if (Vision::Key.IsPressed(VGLK_J)) { ChangeScaling(0.01);
		} else if (Vision::Key.IsPressed(VGLK_D)) { mModelMain->mCamera->PrintDebug();
		} else if (Vision::Key.IsPressed(VGLK_F)) { mModelMain->mMap->PrintDebug();
		} else if (Vision::Key.IsPressed(VGLK_G)) { mModelMain->mEntities->PrintDebug();
		} else if (Vision::Key.IsPressed(VGLK_U)) { shouldAR = true;
		} else if (Vision::Key.IsPressed(VGLK_Y)) { shouldAR = false; external = false; mModelMain->mCamera->EnableInput();
		} else if (Vision::Key.IsPressed(VGLK_I)) { external = true;
		}
	}

	if(Vision::Key.IsPressed(VGLK_HOME)) {
		mMenuController->spMainDlg->SetVisible(false);
		mMenuController->spMainDlg->SetDialogResult(VGUIManager::ID_OK);
		mMenuController->spGUIContext->SetShowCursor(false);
		mModelMain->mCamera->EnableInput();
	}
}

void InputController::ChangeModelPositionSlightly(float x, float y) {
	mModelMain->mEntities->xOffset += x;
	mModelMain->mEntities->yOffset += y;
	
	// update the position of the objects
	for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->gameObjects.begin(); ii != mModelMain->mEntities->gameObjects.end(); ++ii ) {
		(*ii).second->SetPosition((*ii).second->GetPosition().x + x, (*ii).second->GetPosition().y + y, (*ii).second->GetPosition().z);
	}	
}

void InputController::ChangeScaling(float scale) {
	mModelMain->mEntities->scaling += scale;

	if(mModelMain->mEntities->scaling < mModelMain->mEntities->scaleThreshold) { mModelMain->mEntities->scaling = mModelMain->mEntities->scaleThreshold; }

	// update the scale of the objects
	for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->gameObjects.begin(); ii != mModelMain->mEntities->gameObjects.end(); ++ii ) {
		if((*ii).second->GetScaling() != hkvVec3(0))
			(*ii).second->SetScaling(mModelMain->mEntities->scaling);
	}
}