#pragma once

#include "ModelMain.h"

class MenuController
{
public:
	MenuController(ModelMain *pModelMain);
	~MenuController(void);

	bool mShouldQuit, mShouldInit;
	std::string mIpAdress;
	int mPortNumber;

	ModelMain					*mModelMain;
	VSmartPtr<VGUIMainContext>	spGUIContext;
	VDialogPtr					spMainDlg;
	VisSampleAppPtr				spApp;

	void Init();
	void Update();

	void ToggleRangeObjectsVisible(bool visible);
	void ToggleAirCorridorsVisible(bool visible);
	void SetupIMBConnection(std::string ipAdress, int port);
};

