#include "GameApplicationPCH.h"
#include "MenuController.h"
#include <sstream>  

MenuController::MenuController(ModelMain *pModelMain)
{
	mShouldQuit = false;
	mShouldInit = false;
	mModelMain	= pModelMain;
}

MenuController::~MenuController(void)
{

}

void MenuController::Update() {
	int dialogResult = spMainDlg->GetDialogResult();
	
	if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWRANGES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX1"));
		VASSERT(checkbox);
		mModelMain->mEntities->mShowDropLines = checkbox->IsChecked();
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SHOWDROPLINES")) {
		VCheckBox *checkbox = (VCheckBox *) spMainDlg->Items().FindItem(VGUIManager::GetID("CHECKBOX2"));
		VASSERT(checkbox);
		ToggleAirCorridorsVisible(checkbox->IsChecked());
	} else if (dialogResult == VGUIManager::GlobalManager().GetID("SCALE")) {
		VSliderControl *slider = (VSliderControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("SCALE_SLIDER"));
		VASSERT(slider);
		mModelMain->mEntities->scaling = slider->GetValue() / 100;
		
		std::stringstream sliderText;
		std::string text;
		int wholeNumber = slider->GetValue();
		sliderText << wholeNumber;
		sliderText >> text;

		VTextLabel *textLabel = (VTextLabel *) spMainDlg->Items().FindItem(VGUIManager::GetID("SCALE_VALUE"));
		textLabel->SetText(text.c_str());

		for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->gameObjects.begin(); ii != mModelMain->mEntities->gameObjects.end(); ++ii ) {
			(*ii).second->SetScaling(mModelMain->mEntities->scaling);
		}
	} else if ((dialogResult == VGUIManager::GlobalManager().GetID("HIDEGUI"))) {
		spMainDlg->SetVisible(false);
		spGUIContext->SetShowCursor(false);
		spMainDlg->SetDialogResult(VGUIManager::ID_OK);

		mModelMain->mCamera->EnableInput();
	} else if ((dialogResult == VGUIManager::GlobalManager().GetID("LOAD_SCENARIO"))) {
		VTextControl *slider = (VTextControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("SCENARIO_TEXT"));
		spApp->LoadScene(slider->GetText());
		spMainDlg->SetDialogResult(VGUIManager::ID_OK);
	} else if ((dialogResult == VGUIManager::GlobalManager().GetID("LAUNCH_IMB"))) {
		VTextControl *ipAdress = (VTextControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("IMB_IP"));
		VTextControl *portNumber = (VTextControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("IMB_PORT"));

		SetupIMBConnection(ipAdress->GetText(), atoi(portNumber->GetText()));
		spMainDlg->SetDialogResult(VGUIManager::ID_OK);
	} else if ((dialogResult == VGUIManager::GlobalManager().GetID("UPDATE_TABLE"))) {
		VTextControl *height = (VTextControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("TABLE_HEIGHT"));
		VTextControl *width = (VTextControl *) spMainDlg->Items().FindItem(VGUIManager::GetID("TABLE_WIDTH"));
		
		mModelMain->mMap->mTableHeight = atof(height->GetText()) / 100;
		mModelMain->mMap->mTableWidth = atof(width->GetText()) / 100;

		spMainDlg->SetDialogResult(VGUIManager::ID_OK);
	} else if(dialogResult == VGUIManager::ID_CANCEL) {
		mShouldQuit = true;
	}
}

void MenuController::ToggleRangeObjectsVisible(bool visible) {
	for(std::list<VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->rangeObjects.begin(); ii != mModelMain->mEntities->rangeObjects.end(); ++ii ) {
		if(!visible) (*ii)->SetScaling(0);
		else (*ii)->SetScaling(mModelMain->mEntities->scaling);
	}
}

void MenuController::ToggleAirCorridorsVisible(bool visible) {
	for(std::list<VisBaseEntity_cl*>::iterator ii = mModelMain->mEntities->airCorridors.begin(); ii != mModelMain->mEntities->airCorridors.end(); ++ii ) {
		if(!visible) (*ii)->SetScaling(0);
		else (*ii)->SetScaling(mModelMain->mEntities->scaling);
	}	
}

void MenuController::SetupIMBConnection(std::string ipAdress, int port) {
	mShouldInit		= true;
	mIpAdress		= ipAdress;
	mPortNumber		= port;
}
