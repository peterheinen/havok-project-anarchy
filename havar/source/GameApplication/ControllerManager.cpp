#include "GameApplicationPCH.h"
#include "ControllerManager.h"

ControllerManager::ControllerManager(ModelMain *pModelMain)
{
	mModelMain			= pModelMain;
	mCameraController	= new CameraController(mModelMain);
	mDataController		= new DataController(mModelMain);
	mMenuController		= new MenuController(mModelMain);

	mInputController	= new InputController(mModelMain, mMenuController, mDataController);
}

ControllerManager::~ControllerManager(void)
{
}

void ControllerManager::Init(GUI* gui) {
	mMenuController->spGUIContext	= gui->spGUIContext;
	mMenuController->spMainDlg		= gui->spMainDlg;
}

void ControllerManager::Update() {
	mInputController->Update();
	mMenuController->Update();
	mDataController->Update();

	if(mMenuController->mShouldInit) {
		mDataController->Init(mMenuController->mIpAdress, mMenuController->mPortNumber);
	}

	if(mInputController->shouldAR) {
		mCameraController->mARCamera->Init();
		mCameraController->Update();
	} else if(mInputController->external) {
		mModelMain->mCamera->mCamera->SetPosition(*mModelMain->mCamera->externalCameraPosition);
		mModelMain->mCamera->mCamera->SetOrientation(*mModelMain->mCamera->externalCameraOrientation);
		mModelMain->mCamera->DisableInput();
		//mModelMain->mCamera->EnableInput();
	}
}