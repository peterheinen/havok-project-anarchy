#include "GameApplicationPCH.h"
#include "CameraController.h"

CameraController::CameraController(ModelMain *pModelMain)
{
	mModelMain	= pModelMain;
	mARCamera	= new ARCamera(pModelMain);
}

CameraController::~CameraController(void)
{
}

void CameraController::Update() {
	//if(MODE_AR) 
	if(mARCamera->mInit)
		mARCamera->Update();

}