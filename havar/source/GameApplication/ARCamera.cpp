#include "GameApplicationPCH.h"
#include "ARCamera.h"

//ALVAR includes
#include "MarkerDetector.h" // Marker detector
#include "CaptureFactory.h" // Video capturing
#include "MultiMarker.h"
#include "Shared.h"

alvar::Capture*								capture;
alvar::Camera								camera;
alvar::MarkerDetector<alvar::MarkerData>	markerDetector;
alvar::Pose									pose;
alvar::MultiMarker							multi_marker;	

// Size of the marker
// The size is an "abstract value" for library, but using normal, logical values (mm, cm, m, inch) will help 
// understanding the model scaling and positioning in human point of view.

class BackgroundImageHelper : public IVisCallbackHandler_cl
{
public:
  BackgroundImageHelper( VTextureObject* pTexture )
    : IVisCallbackHandler_cl()
    , m_spTexture( pTexture )
  {
    v2TopLeft = hkvVec2::ZeroVector();

    // Determine the bottom right corner for our fullscreen quad.
    VVideoConfig *pVideoConfig = Vision::Video.GetCurrentConfig();
    v2BottomRight = hkvVec2( (float)pVideoConfig->m_iXRes, (float)pVideoConfig->m_iYRes );

    // Listen to render hook callbacks.
    Vision::Callbacks.OnRenderHook += this;
  }

  virtual ~BackgroundImageHelper()
  {
    // Unregister from render hook callbacks.
    Vision::Callbacks.OnRenderHook -= this;
  }

  VTextureObject* GetTexture() const
  {
    return m_spTexture;
  }

  void OnHandleCallback( IVisCallbackDataObject_cl* pData ) HKV_OVERRIDE
  {
    if ( pData->m_pSender == &Vision::Callbacks.OnRenderHook )
    {
      VisRenderHookDataObject_cl* pRenderHookData = ( VisRenderHookDataObject_cl* ) pData;

      // Only perform rendering in the pre-occlusion test pass
      if ( pRenderHookData->m_iEntryConst != VRH_PRE_OCCLUSION_TESTS )
      {
        return;
      }

      // Draw our texture as fullscreen quad.
      IVRender2DInterface *pRenderer = Vision::RenderLoopHelper.BeginOverlayRendering();
      pRenderer->DrawTexturedQuad( v2TopLeft, v2BottomRight, m_spTexture, hkvVec2( 0.0f, 0.0f ), hkvVec2( 1.0f, 1.0f ), V_RGBA_WHITE, RENDERSTATEFLAG_NOWIREFRAME );
      Vision::RenderLoopHelper.EndOverlayRendering();
    }
  }

private:
  VTextureObjectPtr m_spTexture;
  hkvVec2 v2TopLeft;
  hkvVec2 v2BottomRight;
};

BackgroundImageHelper *pBackgroundImageHelper = NULL;

ARCamera::ARCamera(ModelMain *pModelMain)
{
	mModelMain = pModelMain;
	mInit = false;
	mMarkerInit = true;
	invertImage = true;
	mTableHeight = 0.498094;
	mTableWidth  = 0.885444;
}

ARCamera::~ARCamera(void)
{
	if(capture){
		capture->stop();
		delete capture;
	}

	delete pBackgroundImageHelper;
	pBackgroundImageHelper = NULL;
}

void ARCamera::Init() {
	if(!mInit) {
		mInit = true;
		IVRendererNode* pRenderer = Vision::Renderer.GetRendererNode(0); 
		pRenderer->GetViewProperties()->setFov(60, 0);
		pRenderer->OnViewPropertiesChanged();
	
		mModelMain->mCamera->DisableInput();

		alvar::CaptureFactory *factory = alvar::CaptureFactory::instance();
		alvar::CaptureFactory::CaptureDeviceVector devices = factory->enumerateDevices();

		// Check to ensure that a device was found
		if (devices.size() > 0) {
			capture = factory->createCapture(devices.front());
		}

		mCameraCorrector = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3 (0,0,0), "");
		mCameraCorrector->SetScaling(0.0f);
		mCameraCorrector->SetOrientation(0, 0, 0);

		mModelMain->mCamera->mCamera->SetOrientation(90, -90, 0);
		mModelMain->mCamera->mCamera->AttachToParent(mCameraCorrector);

		if (capture && capture->start()) {
			// Let's capture one frame to get video resolution
			IplImage *tempImg = capture->captureImage();
		
			//Create space for outputs rgb and its separate channels, r, g and b
			IplImage* goodImg = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 4);    //rgb
			IplImage* r = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //r
			IplImage* g = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //g
			IplImage* b = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //b
			IplImage* a = cvCreateImage(cvGetSize(tempImg), IPL_DEPTH_8U, 1);    //b

			//VisBaseEntity_cl* tmpObject = Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3(0), "Models\\Misc\\boeing_737.model");
			//tmpObject->SetScaling(0.01f);

			cvSplit(tempImg, b, g, r, NULL);
			cvMerge(r,g,b,a,goodImg);

			videoXRes = goodImg->width;
			videoYRes = goodImg->height;

			// Create an empty texture and fill it with 32bit image data from memory.
			VTextureObject *pTexture = Vision::TextureManager.CreateEmpty2DTextureObject( "<background>", videoXRes, videoYRes );
			pTexture->EnsureLoaded();
			pTexture->UpdateRect( 0, 0, 0, videoXRes, videoYRes, -1, goodImg->imageData, V_TEXTURE_LOCKFLAG_DISCARDABLE );

			// Create a background image helper.
			pBackgroundImageHelper = new BackgroundImageHelper( pTexture );

			cvReleaseImage(&goodImg);
			cvReleaseImage(&r);
			cvReleaseImage(&g);
			cvReleaseImage(&b);
			cvReleaseImage(&a);

			// Calibration. See manual and ALVAR internal samples how to calibrate your camera
			// Calibration will make the marker detecting and marker pose calculation more accurate.
			if (! camera.SetCalib("calib.xml", videoXRes, videoYRes)) {
				camera.SetRes(videoXRes, videoYRes);
			}

			if(!multi_marker.Load("markers.xml", alvar::FILE_FORMAT_XML)) {
				mMarkerInit = false;
			}
		}
	}
}

void ARCamera::Update() {
	mTableHeight = mModelMain->mMap->mTableHeight;
	mTableWidth  = mModelMain->mMap->mTableWidth;
	mModelMain->mCamera->DisableInput();
	double scaleFactor = (((mModelMain->mMap->realHeightDistance / mTableHeight) + (mModelMain->mMap->realWidthDistance  / mTableWidth)) / 2) / 100;

	//Call the rendering function over and over again.
	IplImage *image = capture->captureImage(); 
	
	//Create space for outputs rgb and its separate channels, r, g and b
	IplImage* goodImg = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 4);    //rgb
	IplImage* r = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //r
	IplImage* g = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //g
	IplImage* b = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //b
	IplImage* a = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 1);    //a

	cvSplit(image, r, g, b, NULL);
	cvMerge(b, g, r, a, goodImg);

	pBackgroundImageHelper->GetTexture()->UpdateRect( 0, 0, 0, goodImg->width, goodImg->height, -1, goodImg->imageData, V_TEXTURE_LOCKFLAG_DISCARDABLE );
	uint64 uiEndTime = VGLGetTimer();

	cvReleaseImage(&goodImg);
	cvReleaseImage(&r);
	cvReleaseImage(&g);
	cvReleaseImage(&b);
	cvReleaseImage(&a);

	// Check if we need to change image origin and is so, flip the image.
	bool flip_image = (image->origin?true:false);
	if (flip_image) {
		cvFlip(image);
		image->origin = !image->origin;
	}

	if(invertImage) {
 		int height = image->height;
		int widthStep = image->widthStep;
		unsigned char* data = (unsigned char *)image->imageData;

		for(int i = 0; i < widthStep * height; i++) {
			data[i] = 255 - data[i];
		}
	}

	double error = -1;
    if (markerDetector.Detect(image, &camera, true, false)) {
        error = multi_marker.Update(markerDetector.markers, &camera, pose);
        multi_marker.SetTrackMarkers(markerDetector, &camera, pose, image);
        markerDetector.DetectAdditional(image, &camera, false);
        error = multi_marker.Update(markerDetector.markers, &camera, pose);
    }

	if ((error >= 0) && (error < 5)) {		 
		CvMat *invertedMatrix = cvCreateMat(4, 4, CV_64FC1); 
		pose.GetMatrix(invertedMatrix);

		hkvMat4* havokMatrix = new hkvMat4(cvmGet(invertedMatrix, 0, 0),cvmGet(invertedMatrix, 0, 1),cvmGet(invertedMatrix, 0, 2),cvmGet(invertedMatrix, 0, 3),
			cvmGet(invertedMatrix, 1, 0),cvmGet(invertedMatrix, 1, 1),cvmGet(invertedMatrix, 1, 2),cvmGet(invertedMatrix, 1, 3),
			cvmGet(invertedMatrix, 2, 0),cvmGet(invertedMatrix, 2, 1),cvmGet(invertedMatrix, 2, 2),cvmGet(invertedMatrix, 2, 3),
			cvmGet(invertedMatrix, 3, 0),cvmGet(invertedMatrix, 3, 1),cvmGet(invertedMatrix, 3, 2),cvmGet(invertedMatrix, 3, 3));

		havokMatrix->invert();

		mModelMain->mCamera->mCamera->SetLocalOrientation(90, -90, 0);
		mModelMain->mCamera->mCamera->AttachToParent(mCameraCorrector);

		float yaw, pitch, roll;
		havokMatrix->getAsEulerAngles(roll,pitch,yaw);
		hkvVec3 position = havokMatrix->getTranslation();
		
		//Vision::Message.Print(1, 10, 300, "Init correct?: %i", mMarkerInit);
		//Vision::Message.Print(1, 10, 320, "Computed position: %f %f %f", position.x, position.y, position.z);
		//Vision::Message.Print(1, 10, 340, "Computed orientation: %f %f %f", yaw,pitch,roll);
		//Vision::Message.Print(1, 10, 360, "Scale: %f", scaleFactor);

		mCameraCorrector->SetOrientation(yaw, pitch, roll); 
		mModelMain->mCamera->mCamera->SetPosition(position.x * scaleFactor + mModelMain->mMap->xOffset + mModelMain->mMap->manualXOffset, position.y * scaleFactor + mModelMain->mMap->yOffset + mModelMain->mMap->manualYOffset, position.z * scaleFactor);
		//mModelMain->mCamera->mCamera->SetPosition(position.x, position.y, position.z);
	}

	// In case we flipped the image, it's time to flip it back 
	if (flip_image) {
		cvFlip(image);
		image->origin = !image->origin;
	}	
}
