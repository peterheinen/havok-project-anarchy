#pragma once

#include "Entities.h"
#include "HavokCamera.h"
#include "Map.h"

class ModelMain
{
public:
	ModelMain(void);
	~ModelMain(void);

	void Init();
	void Update();
	void PrintDebug();

	Entities	*mEntities;
	Map			*mMap;
	HavokCamera		*mCamera;
};

