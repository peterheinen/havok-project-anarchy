#include "GameApplicationPCH.h"
#include "Util.h"
#define _USE_MATH_DEFINES
#include <math.h>

static float DEGREE_TO_RAD = (M_PI / 180);
static float RAD_TO_DEGREE = (180 / M_PI);
static double EQUATOR_RADIUS = 6378137;

Util::Util(void)
{
}


Util::~Util(void)
{
}

hkvVec3d Util::ConvertLatLongAltToHavokCoords(double latitude, double longitude, double altitude) {
	// Get the Vision Coordinates System
	VisCoordinateSystem_cl* cs = (VisCoordinateSystem_cl*)Vision::World.GetCoordinateSystem();

	// Convert input coordinates to radians
	hkvVec3d geodetic;
	geodetic.set(latitude * DEGREE_TO_RAD, longitude * DEGREE_TO_RAD, altitude);

	// Convert to Vision coordinates
	hkvVec3d cartesian;
	cs->ProjectCoordinate(geodetic, cartesian);

	hkvVec3d refPos;
	Vision::World.GetCoordinateSystem()->GetSceneReferencePosition(refPos);
	cartesian.x = cartesian.x - refPos.x;
	cartesian.y = cartesian.y - refPos.y;
	cartesian.z = cartesian.z - refPos.z;

	return cartesian;
}

double Asinh(double x) { return log(x + sqrt(x * x + 1)); }

hkvVec3d Util::ConvertMetersToHavokCoords(double metersX, double metersY) {
	// Get the Vision Coordinates System
	double latAngle, lonAngle;

	latAngle = atan(sinh(metersX / EQUATOR_RADIUS));
	lonAngle = metersY / EQUATOR_RADIUS;

	latAngle *= RAD_TO_DEGREE;
	lonAngle *= RAD_TO_DEGREE;

	hkvVec3d geodetic;
	geodetic.set(latAngle, lonAngle, 0);

	return ConvertLatLongAltToHavokCoords(geodetic.x, geodetic.y, geodetic.z);
}