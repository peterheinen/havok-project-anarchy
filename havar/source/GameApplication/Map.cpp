#include "GameApplicationPCH.h"
#include "Map.h"


Map::Map(void)
{
	clipBox = std::vector<double>(4);
	mTableHeight = 0.498094f;
	mTableWidth  = 0.885444f;
	manualXOffset = 0.75;
	manualYOffset = -4.75;
}

Map::~Map(void)
{
}

void Map::PrintDebug() {
	Vision::Message.Print(1, 10, 40, "Map computed offset: %f %f", xOffset, yOffset);
	Vision::Message.Print(1, 10, 60, "Map width and height difference in meters: %f %f", realWidthDistance, realHeightDistance);
	Vision::Message.Print(1, 10, 80, "Table Width and height: %f %f", mTableWidth, mTableHeight);
	Vision::Message.Print(1, 10, 100, "Manual offset AR: %f %f", manualXOffset, manualYOffset);
}