#pragma once

#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/GUI/vGUI.hpp>
#include <Vision/Runtime/EnginePlugins/VisionEnginePlugin/GUI/VWindowBase.hpp>

class GUI
{
public:
	GUI(void);
	virtual ~GUI(void);

	VSmartPtr<VGUIMainContext> spGUIContext;
	VDialogPtr spMainDlg;

	void InitGUI();
	void DeInitGUI();
	void ProcessGUIResult();
};

