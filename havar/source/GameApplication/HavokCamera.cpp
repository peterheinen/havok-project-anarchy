#include "GameApplicationPCH.h"
#include "HavokCamera.h"


HavokCamera::HavokCamera()
{
	externalCameraOrientation = new hkvVec3(0);
	externalCameraPosition = new hkvVec3(0);
}


HavokCamera::~HavokCamera(void)
{
}

void HavokCamera::SetCamera(VisBaseEntity_cl	*pCamera) {
	mCamera = pCamera;
	mMouseCamera = (VisMouseCamera_cl*) pCamera;

	mMoveSpeed		= mMouseCamera->m_fMoveSpeed;
	mSensitivity	= mMouseCamera->m_fSensitivity;
	mUpDownSpeed	= mMouseCamera->m_fUpDownSpeed;
}

void HavokCamera::DisableInput() {
	mMouseCamera->m_fSensitivity	= 0.0f;
	mMouseCamera->m_fMoveSpeed		= 0.0f;
	mMouseCamera->m_fUpDownSpeed	= 0.0f;
}

void HavokCamera::EnableInput() {
	mMouseCamera->m_fSensitivity	= mSensitivity;
	mMouseCamera->m_fMoveSpeed		= mMoveSpeed;
	mMouseCamera->m_fUpDownSpeed	= mUpDownSpeed;
}

void HavokCamera::PrintDebug() {
	Vision::Message.Print(1, 10, 20, "Camera Information \nCamera position: %f %f %f  \nCamera oriŽntation %f %f %f ", 
		mCamera->GetPosition().x, mCamera->GetPosition().y, mCamera->GetPosition().z, mCamera->GetOrientation().x, mCamera->GetOrientation().y, mCamera->GetOrientation().z );
}