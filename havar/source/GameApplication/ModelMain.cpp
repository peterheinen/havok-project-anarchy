#include "GameApplicationPCH.h"
#include "ModelMain.h"


ModelMain::ModelMain(void)
{
	mEntities	= new Entities();
	mMap		= new Map();
	mCamera		= new HavokCamera();
}

ModelMain::~ModelMain(void)
{

}

void ModelMain::Init() {

}

void ModelMain::Update() {
	if(mEntities->mShowDropLines) {
		for(std::map<std::string, VisBaseEntity_cl*>::iterator ii = mEntities->gameObjects.begin(); ii !=  mEntities->gameObjects.end(); ++ii ) {
			if((*ii).second->GetPosition().z > 0) {
					Vision::Game.DrawSingleLine((*ii).second->GetPosition().x,(*ii).second->GetPosition().y, 0,
							(*ii).second->GetPosition().x,(*ii).second->GetPosition().y,(*ii).second->GetPosition().z, V_RGBA_RED);
			}
			
			Vision::Message.DrawMessage3D("Naam", hkvVec3((*ii).second->GetPosition().x, (*ii).second->GetPosition().y, (*ii).second->GetPosition().z + 3));
		}
	}
}

void ModelMain::PrintDebug() {

}