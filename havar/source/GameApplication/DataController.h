#pragma once

#include "DataConverter.h"
#include "MenuController.h"
#include "Util.h"
#include "ModelMain.h"
#include <string>

class DataController
{
public:
	DataController(ModelMain *pModelMain);
	~DataController();

	void Init(std::string ipAddres, int port);
	void Update();

	bool mIsInitialized;
	std::string ipAdressConnect;
	int myPort, connected;


	Util			*mUtil;
	DataConverter	*mDataConverter;
	ModelMain		*mModelMain;
	MenuController	*mMenuController;
};