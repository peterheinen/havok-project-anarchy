#include "GameApplicationPCH.h"
#include "Entities.h"


Entities::Entities(void)
{
	scaling = 1.0;
	scaleThreshold = 1.0;
	mShowDropLines = true;
	xOffset = 2.298673f;
	yOffset = -6.801323f;
}


Entities::~Entities(void)
{
}

void Entities::PrintDebug() {
	Vision::Message.Print(1, 10, 20, "Printing Entities info: ");
	Vision::Message.Print(1, 10, 40, "Number of entities found: %i", gameObjects.size());
	Vision::Message.Print(1, 10, 60, "Types of entities without a model: %s", myTypes.c_str());
	Vision::Message.Print(1, 10, 80, "Scale amount: %f", scaling);
	Vision::Message.Print(1, 10, 100, "Offset: %f %f", xOffset, yOffset);
}