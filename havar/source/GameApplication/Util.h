#pragma once
class Util
{
public:
	Util(void);
	~Util(void);

	hkvVec3d ConvertLatLongAltToHavokCoords(double latitude, double longitude, double altitude);
	hkvVec3d ConvertMetersToHavokCoords(double metersX, double metersY);
};

