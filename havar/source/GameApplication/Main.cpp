/*
 *
 * Confidential Information of Telekinesys Research Limited (t/a Havok). Not for disclosure or distribution without Havok's
 * prior written consent. This software contains code, techniques and know-how which is confidential and proprietary to Havok.
 * Product and Trade Secret source code contains trade secrets of Havok. Havok Software (C) Copyright 1999-2013 Telekinesys Research Limited t/a Havok. All Rights Reserved. Use of this software is subject to the terms of an end user license agreement.
 *
 */

//  Basic template to base a project EXE on.
//	This template version contain a simple demo Action in the TemplateAction.cpp file
//
//	TODO: Needs config file for properties
//

#include "GameApplicationPCH.h"
#include <string>
#include <iostream>
#include <stdlib.h>
#include <hkvQuat.h> 

#include "DataConverter.h"
#include "ModelMain.h"
#include "ControllerManager.h"
#include "GUI.h"

//============================================================================================================
//	Properties for start up. Some of the settings are not relevant for mobile devices
//============================================================================================================
int windowSizeX		= 1024;					// Set the Window size X if not in fullscreen.
int windowSizeY		= 768;					// Set the Window size Y if not in fullscreen.
int windowPosX		= 500;					// Set the Window position X if not in fullscreen.
int windowPosy		= 50;					// Set the Window position Y if not in fullscreen.

char name[]			= "TNO Havok";						// Name to be displayed in the windows title bar.
char StartUpScene[]	= "Scenes/Maarnehuizen.vscene";		// Set the location of your start up scene.

std::vector<double> clipBox(4);		
VisBaseEntity_cl	*pCamera; 

GUI					*mGui;
ControllerManager	*mControllerManager;
ModelMain			*mModelMain;

//============================================================================================================
//	Action  Declaration  
//============================================================================================================
//	Declare a module to put all our actions into
	DECLARE_THIS_MODULE(g_sampleModule, MAKE_VERSION(1,0),
										"Project Template Module",
										"Trinigy",
										"This module contains this projects actions", NULL );
                
VisSampleAppPtr spApp;

//---------------------------------------------------------------------------------------------------------
//	Init function. Here we trigger loading our scene
//---------------------------------------------------------------------------------------------------------
VISION_INIT
{
  	//	Create our new application.
	spApp = new VisSampleApp();
	
	//	set the initial starting position of our game window
	//	and other properties if not in fullscreen. This is only relevant on windows
#if defined(WIN32)
  spApp->m_appConfig.m_videoConfig.m_iXPos = windowPosX;
	spApp->m_appConfig.m_videoConfig.m_iYPos = windowPosy;
	spApp->m_appConfig.m_videoConfig.m_szWindowTitle = name;
#endif

	//	Set the exe directory the current directory
	VisionAppHelpers::MakeEXEDirCurrent();

	// Set the paths to our stand alone version to over ride the VisSAampleApp paths.
  // The paths are platform dependent
#if defined(WIN32)
  const VString szRoot = "..\\..\\..\\..";
  //Vision::File.AddDataDirectory( szRoot );
  Vision::File.AddDataDirectory( szRoot + "\\Assets\\Default" );
  Vision::File.AddDataDirectory( szRoot + "\\Assets\\TNO" );
  Vision::File.AddDataDirectory( szRoot + "\\Assets\\Havar" );
  Vision::File.AddDataDirectory( szRoot + "\\Data\\Vision\\Base" );
  Vision::File.AddDataDirectory( szRoot + "\\Assets\\Models\\Misc\\Textures" );
#elif defined(_VISION_ANDROID)
  VString szRoot = VisSampleApp::GetApkDirectory();
  szRoot += "?assets";
  Vision::File.AddDataDirectory( szRoot + "\\Assets" );
  //Vision::File.AddDataDirectory( szRoot + "/Data/Vision/Base" ); //we don't have to call this line, since the base data 
                                                                   //folder is added automatically in the VisSampleApp init function
#endif


	//load plugins dynamically (windows only)
	#if defined(_DLL) && !defined(VBASE_LIB)
		//Load the plugins that the scene referenced.
		VSceneLoader ldr;
		ldr.LoadEnginePlugins( StartUpScene, Vision::File.GetManager() );
	#endif

	spApp->LoadVisionEnginePlugin();

	//use the following line if you link statically. e.g. for mobile. 
	//You can remove this line when developing for windows only
	VISION_PLUGIN_ENSURE_LOADED(GamePlugin);

	//	Init the application and point it to the start up scene.
	if (!spApp->InitSample( "", StartUpScene, VSampleFlags::VSAMPLE_INIT_DEFAULTS|VSampleFlags::VSAMPLE_CUSTOMDATADIRECTORIES,windowSizeX,windowSizeY))
		return false;

	mGui = new GUI();
	mGui->InitGUI(); 

	mModelMain = new ModelMain();
	mModelMain->Init();

	mControllerManager = new ControllerManager(mModelMain);
	mControllerManager->Init(mGui);
	mControllerManager->mMenuController->spApp = spApp;

    pCamera = spApp->EnableMouseCamera();
	pCamera->SetOrientation( hkvVec3( 60, 20, 0));
    pCamera->SetPosition( hkvVec3(6010, 4030, 70));

	mModelMain->mCamera->SetCamera(pCamera);

	return true;
}

//---------------------------------------------------------------------------------------------------------
//	Gets called after the scene has been loaded
//---------------------------------------------------------------------------------------------------------

VISION_SAMPLEAPP_AFTER_LOADING
{

	/*IVRendererNode* pRenderer = Vision::Renderer.GetRendererNode(0); 
	pRenderer->GetViewProperties()->setFov(45, 0);
	pRenderer->OnViewPropertiesChanged();*/
	
	/*alvar::CaptureFactory *factory = alvar::CaptureFactory::instance();
    alvar::CaptureFactory::CaptureDeviceVector devices = factory->enumerateDevices();

    // Check to ensure that a device was found
    if (devices.size() > 0) {
        //capture = factory->createCapture(devices.front());
    }*/

	//cameraCorrector		= Vision::Game.CreateEntity("VisBaseEntity_cl", hkvVec3 (0,0,0), "");
	//cameraCorrector->SetScaling(0.0f);
	//cameraCorrector->SetOrientation(0, 0, 0);

	//pCamera->SetOrientation(90, -90, 0);
	//pCamera->AttachToParent(cameraCorrector);

	//dc = new DataConverter();
	//InitGUI();

	/*amsterdamPrefab = VPrefabManager::GlobalManager().LoadPrefab("Prefabs\\Amsterdam.vprefab");
	pPrefab			= VPrefabManager::GlobalManager().LoadPrefab("Prefabs\\ExplosionImpact.vprefab");*/

    //---------------------------------------------------------------------------------------------------------
    //	register the action module with the vision engine action manager
    //	only after that will the action becomes available in the console.
    //---------------------------------------------------------------------------------------------------------
    VActionManager * pManager = Vision::GetActionManager ();
    pManager->RegisterModule ( &g_sampleModule );

    //	Set to true to open the console at startup and print some data to the display
    Vision::GetConsoleManager()->Show( false );
    pManager->Print( "Welcome to the console!" );
    pManager->Print( "This module is called '%s'", g_sampleModule.GetName() );
    pManager->Print( "Type in 'help' for a list of all actions" );
    pManager->Print( "Type in 'myAction' to test this projects demo action" );
}

//---------------------------------------------------------------------------------------------------------
//	main loop of the application until we quit
//---------------------------------------------------------------------------------------------------------
VISION_SAMPLEAPP_RUN
{							
	mControllerManager->Update();
	mModelMain->Update();

	// Look if Havok wants to run and if the GUI did not get a kill command
	return spApp->Run() && !mControllerManager->mMenuController->mShouldQuit;// && dialogResult != VGUIManager::ID_CANCEL;
}


VISION_DEINIT
{
	// Time to close the system
	/*if(capture){
		capture->stop();
		delete capture;
	}

	delete pBackgroundImageHelper;
	pBackgroundImageHelper = NULL;
	*/
//	DeInitGUI();
    
	mControllerManager->mCameraController->mARCamera->~ARCamera();

	// Deinit the application
    spApp->DeInitSample();
    spApp = NULL;
    return true;
}

VISION_MAIN_DEFAULT

/*
 * Havok SDK - Base file, BUILD(#20130523)
 * 
 * Confidential Information of Havok.  (C) Copyright 1999-2013
 * Telekinesys Research Limited t/a Havok. All Rights Reserved. The Havok
 * Logo, and the Havok buzzsaw logo are trademarks of Havok.  Title, ownership
 * rights, and intellectual property rights in the Havok software remain in
 * Havok and/or its suppliers.
 * 
 * Use of this software for evaluation purposes is subject to and indicates
 * acceptance of the End User licence Agreement for this product. A copy of
 * the license is included with this software and is also available from salesteam@havok.com.
 * 
 */